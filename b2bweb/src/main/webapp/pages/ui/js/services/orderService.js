angular
.module('app')
.factory('orderService', orderService);

orderService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function orderService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addOrder:addOrder,
        getNewOrders:getNewOrders,
        addNewQuotes: addNewQuotes,
        getVendorQuotes: getVendorQuotes,
        updateQuote: updateQuote,
    };
    return service;

    function addOrder(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.ADDORDER + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

    function getNewOrders(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.GET_NEWCUSTOMERQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {        
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

    function addNewQuotes(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.VENDORQUOTES + commonService.ADD_VENDORQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

    function getVendorQuotes(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.VENDORQUOTES + commonService.GETVENDORQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {        
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

    function updateQuote(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.VENDORQUOTES + commonService.UPDATEVENDORQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {        
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }
}