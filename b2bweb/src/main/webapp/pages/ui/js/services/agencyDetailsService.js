angular
.module('app')
.factory('agencyDetailsService', agencyDetailsService);

agencyDetailsService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function agencyDetailsService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addvendordetail: addvendordetail
       
    };

    return service;

    function addvendordetail(agencyDetails){
    	console.log('You Called service Method');
        console.log("token",$localStorage.jwtToken);
        var requestParam = {
            vendorid : $localStorage.jwtToken.jti,
            displayname : agencyDetails.displayname,
            businessdesc : agencyDetails.businessdesc
       }
        console.log(requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.AGENCY_SERVICE + commonService.AGENCY_SERVICE_ADD+ '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,               
            headers: { 'Content-Type': undefined }
            /* data : vendorUser*/
        }).success(function (data, status, headers, config) {
            console.log("Vendor DEtails  ", data);
            //$localStorage.token = response.data;
            deferred.resolve(data);
        }).error(function (data, status) {
            console.error('Error while fetching Users    ', data);
            //$localStorage.token = '';
            deferred.reject(data);
        });       
            return deferred.promise;
    }
    
   
}