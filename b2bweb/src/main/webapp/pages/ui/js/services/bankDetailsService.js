angular
.module('app')
.factory('bankDetailsService', bankDetailsService);

bankDetailsService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope', '$cookies'];

function bankDetailsService($http, $q, constantsService, commonService, $localStorage, $rootScope, $cookies) {
    var service = {
        addbankdetail: addbankdetail,
        getbankdetail: getbankdetail,
        uploadFile: uploadFile,
        getImage : getImage
    };

    return service;

    function addbankdetail(bankDetails){
    	console.log('You Called service Method');
        console.log("token",$localStorage.jwtToken);
        var cookiesDetail = $cookies.getObject('userInfoCookies');
        var requestParam = {
            vendorid : cookiesDetail.jti,
            accountholdername : bankDetails.accountholdername,
            accountnumber : bankDetails.accountnumber,
            ifsc : bankDetails.Ifsc,
            bankname : bankDetails.bankname,
            state : bankDetails.state,
            city : bankDetails.city,
            branch : bankDetails.branch,
            addressprooftype : bankDetails.addressprooftype
       }
        console.log(requestParam);
           var deferred = $q.defer();
           $http({
                method : commonService.METHOD_GET,
                url :  constantsService.url + commonService.BANK_SERVICE + commonService.BANK_SERVICE_ADD + '/' + btoa(JSON.stringify(requestParam)),
                transformRequest: angular.identity, 
                transformResponse: angular.identity,               
                headers: { 'Content-Type': undefined }
            }).success(function (data, status, headers, config) {
                console.log("Bank Details   ", data);
                //$localStorage.token = response.data;
                deferred.resolve(data);
            }).error(function (data, status) {
                console.error('Error while fetching Users    ', data);
                //$localStorage.token = '';
                deferred.reject(data);
            });          
            return deferred.promise;
    }

    function getbankdetail(vendorid){
        console.log('You Called service Method');
        var deferred = $q.defer();
           $http({
                method : commonService.METHOD_GET,
                url :  constantsService.url + commonService.BANK_SERVICE + commonService.BANK_SERVICE_GET + '/' + btoa(JSON.stringify(vendorid)),
                transformRequest: angular.identity, 
                transformResponse: angular.identity,               
                headers: { 'Content-Type': undefined }
            }).success(function (data, status, headers, config) {
                console.log("Bank Details   ", data);
                //$localStorage.token = response.data;
                deferred.resolve(data);
            }).error(function (data, status) {
                console.error('Error while fetching Users    ', data);
                //$localStorage.token = '';
                deferred.reject(data);
            });          
            return deferred.promise;
    }
    
    function uploadFile (element, type){
        var formData = new FormData();
        var file = element.files[0];
        var cookiesDetail = $cookies.getObject('userInfoCookies')
        formData.append("file", file);
        formData.append("typename", 'bank');
        formData.append("type", type);
        formData.append("vendorid", cookiesDetail.jti);

        var deferred = $q.defer();
        $http({
                method: 'POST',
                url: constantsService.url + commonService.BANK_SERVICE +'/newDocument',
                headers: { 'Content-Type': undefined}, 
                transformRequest: angular.identity, 
                transformResponse: angular.identity,
                data: formData
            }).success(function (data) {
                console.log("sucesss");
                deferred.resolve(data);
            }).error(function (data) {
                console.error('Error while fetching Users    ');
                deferred.reject(data);
            });
            return deferred.promise;
    };

    function getImage(type){
        var requestParam = {
            name: type
        }
        console.log("type",type);
        console.log(constantsService.url+ commonService.BANK_SERVICE + '/getimage' );
        var deferred = $q.defer();
       $http({
            method : commonService.METHOD_GET,
            url :  constantsService.url+ commonService.BANK_SERVICE + '/getimage' + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status) {
            console.error('Error while fetching Users    ', data);
            deferred.reject(data);
        });
        return deferred.promise;
    }
   
}