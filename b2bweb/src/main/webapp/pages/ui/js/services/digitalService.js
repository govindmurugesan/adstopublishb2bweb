angular.module('app').factory('digitalService', digitalService);

digitalService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function digitalService(constantsService, $q, $http, commonService) {
    
	var service = {
        getDigitalDetail: getDigitalDetail,
        addDigital: addDigital
    };
    return service;

    function getDigitalDetail(parameters){
        var requestParam = {
			offset: parameters.offset == undefined ? '':parameters.offset,
            limit:'',
            filters:{
                categories: (parameters.category.length > 0 && parameters.category != undefined) ? parameters.category : [],
                medium:  (parameters.medium.length > 0 && parameters.medium != undefined) ? parameters.medium : [],
                prisingmodel:  (parameters.prisingmodel.length > 0 && parameters.prisingmodel != undefined) ? parameters.prisingmodel : [],
                languages: (parameters.languages.length > 0 && parameters.languages != undefined) ? parameters.languages : [],
            },
            sortBy: parameters.sortBy,
            recommended:''
        };
        var deferred = $q.defer();
    	
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.DIGITAL + commonService.GET_DIGITAL + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }
    
     function addDigital(requestParam){
        console.log('addRadio service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.DIGITAL + commonService.ADD_DIGITAL + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

}