angular
.module('app')
.factory('homeService', homeService);

homeService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage'];

function homeService($http, $q, constantsService, commonService, $localStorage) {
    var service = {
        login: login,
        forgetpass: forgetpass,
        register: register
    };

    return service;

    function login(user){
	   var deferred = $q.defer();
	   $http({
			method : commonService.METHOD_GET,
			url :  constantsService.url + commonService.LOGIN_SERVICE + commonService.LOGIN_SERVICE_LOGIN + '/' + btoa(JSON.stringify(user)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity			
	   }).success(function (data, status, headers, config) {
				$localStorage.token = headers('token');
				console.log($localStorage.token)
				deferred.resolve(data);
		 }).error(function (data, status) {});  
		return deferred.promise;
    }

	function forgetpass(forgetPassword) {
		var deferred = $q.defer();
		var requestParam = {
			username : forgetPassword.userName
		};
		console.log(JSON.stringify(requestParam));
		$http({
			method : commonService.METHOD_GET,
			url :  constantsService.url + commonService.LOGIN_SERVICE + commonService.LOGIN_SERVICE_FORGOT_PASSWORD + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity, 
			headers: { 'Content-Type': undefined
			}
		}).success(function (data, status, headers, config) {
			console.log("Success");
			deferred.resolve(data);
		}).error(function (data, status) {
			console.log("Error While Fetching");
			deferred.reject(data);
		});     
		return deferred.promise;
	}

   	function register(user){
    	 var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.LOGIN_SERVICE +  commonService.LOGIN_SERVICE_REGISTER +'/' + btoa(JSON.stringify(user)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity, 
            headers: { 'Content-Type': undefined				
            }
        }).success(function (data, status, headers, config) {
        	$localStorage.token = headers('token');
			console.log("Success");
			deferred.resolve(data);
		}).error(function (data, status) {
			console.log("Error While Fetching");
			deferred.reject(data);
		});     
		return deferred.promise;
    }
}