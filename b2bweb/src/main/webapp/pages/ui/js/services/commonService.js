(function() {
'use strict';


	var commonService = {
		METHOD_GET : 'GET',
		METHOD_POST : 'POST',

		// login service
		LOGIN_SERVICE : '/vendor',
		LOGIN_SERVICE_REGISTER : '/register',
		LOGIN_SERVICE_LOGIN : '/login',
		LOGIN_SERVICE_FORGOT_PASSWORD : '/forgetpassword',

		// profile detail service
        VENDOR : '/vendor',
        UPDATE : '/update',
        GET_VENDOR : '/getvendor',

		// bank detail service
		BANK_SERVICE : '/vendorbank',
		BANK_SERVICE_ADD : '/addbankdetail',
		BANK_SERVICE_GET : '/getbyvendorid',

		// business detail service
		BUSINESS_SERVICE : '/vendorbusiness',
		BUSINESS_SERVICE_ADD : '/addbusinessdetail',
		BUSINESS_SERVICE_GET : '/getbyvendorid',

		// agency detail service
		AGENCY_SERVICE : '/vendordetail',
		AGENCY_SERVICE_ADD : '/addvendordetail',

		// radio detail service
		RADIO_SERVICE : '/radio',
		RADIO_SERVICE_GET : '/getradio',
		ADD_RADIO: '/addradio',

		// magazine detail service
		MAGAZINE : '/magazine',
		GET_MAGAZINE : '/getmagazine',
		ADD_MAGAZINE : '/addmagazine',
		UPDATE_MAGAZINE : '/updatemagazine',

		// newspaper detail service
		NEWSPAPER : '/newspaper',
		GET_NEWSPAPER : '/getnewspaper',
		ADD_NEWSPAPER : '/addnewspaper',
		UPDATE_NEWSPAPER : '/updatenewspaper',

		//  airline and airports detail service
		 AIRLINE_AND_AIRPORT : '/airlineandairports',
		 GET_AIRLINE_AND_AIRPORT : '/getairlineandairports',
		 ADD_AIRLINE_AND_AIRPORT : '/addairlineandairports',

		 //  cinema  service
		 CINEMA : '/cinemas',
		 GET_CINEMA : '/getcinemas',
		 ADD_CINEMA : '/addcinemas',
		  //  nontraditional  service
		 NON_TRADITIONAL : '/nontraditional',
		 GET_NON_TRADITIONAL: '/getnontraditional',
		 ADD_NON_TRADITIONAL: '/addnontraditional',

		 //  nontraditional  service
		 OUTDOOR : '/outdoor',
		 GET_OUTDOOR: '/getoutdoor',
		 ADD_OUTDOOR: '/addoutdoor',

		  //  nontraditional  service
		 DIGITAL : '/digital',
		 GET_DIGITAL: '/getdigital',
		 ADD_DIGITAL: '/adddigital',

		 // television detail service
        TELEVISION_SERVICE : '/television',
        TELEVISION_SERVICE_GET : '/gettelevision', 
        ADD_TELEVISION : '/addtelevision',

        // order service
        CUSTOMERQUOTES : '/customerquotes',
         GET_NEWCUSTOMERQUOTES : "/getnewcustomerquotes",
         VENDORQUOTES : "/vendorquotes",
        ADD_VENDORQUOTES : "/addvendorquotes",
        GETVENDORQUOTES : "/getvendorquotes",
        UPDATEVENDORQUOTES : "/updatevendorquotes",



	};	

	angular.module("app").constant('commonService', commonService);

	})();
