angular
.module('app')
.factory('profileService', profileService);

profileService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function profileService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
       addprofiledetail: addprofiledetail,
       getprofiledetail: getprofiledetail
   }
    return service;

    function addprofiledetail(requestParam){
        console.log(requestParam);
           var deferred = $q.defer();
           $http({
                method : commonService.METHOD_GET,
                url :  constantsService.url + commonService.VENDOR + commonService.UPDATE + '/' + btoa(JSON.stringify(requestParam)),
                transformRequest: angular.identity, 
                transformResponse: angular.identity,               
                headers: { 'Content-Type': undefined }
            }).success(function (data, status, headers, config) {
                console.log("profile Details   ", data);
                //$localStorage.token = response.data;
                deferred.resolve(data);
            }).error(function (data, status) {
                console.error('Error while fetching Users    ', data);
                //$localStorage.token = '';
                deferred.reject(data);
            });          
            return deferred.promise;
    }

    function getprofiledetail(vendorid){
        
       
           var deferred = $q.defer();
           $http({
                method : commonService.METHOD_GET,
                url :  constantsService.url + commonService.VENDOR + commonService.GET_VENDOR + '/' + btoa(JSON.stringify(vendorid)),
                transformRequest: angular.identity, 
                transformResponse: angular.identity,               
                headers: { 'Content-Type': undefined }
            }).success(function (data, status, headers, config) {
                console.log("profile Details   ", data);
                //$localStorage.token = response.data;
                deferred.resolve(data);
            }).error(function (data, status) {
                console.error('Error while fetching Users    ', data);
                //$localStorage.token = '';
                deferred.reject(data);
            });          
            return deferred.promise;
    }

    
   
   }