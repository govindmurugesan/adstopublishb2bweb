angular
.module('app')
.factory('businessDetailsService', businessDetailsService);

businessDetailsService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope', '$cookies'];

function businessDetailsService($http, $q, constantsService, commonService, $localStorage, $rootScope, $cookies) {
    var service = {
        addbusinessdetail: addbusinessdetail,
        getbusinessdetail: getbusinessdetail,
        uploadFile : uploadFile,
        getImage : getImage
    };

    return service;

    function addbusinessdetail(requestParam){
    	
    	var cookiesDetail = $cookies.getObject('userInfoCookies');
        requestParam.vendorid = cookiesDetail.jti;
           
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url+ commonService.BUSINESS_SERVICE + commonService.BUSINESS_SERVICE_ADD + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
			console.log("Business Details  ", data);
			//$localStorage.token = response.data;
			deferred.resolve(data);
		}).error(function (data, status) {
			console.error('Error while fetching Users    ', data);
			//$localStorage.token = '';
			deferred.reject(data);
		});        
        return deferred.promise;
    }
    
    function getbusinessdetail(vendorid){
        console.log('You Called service Method');
        var requestParam = {
            vendorid : $localStorage.jwtToken.jti,
        };
        console.log(requestParam);
        var deferred = $q.defer();
       $http({
            method : commonService.METHOD_GET,
            url :  constantsService.url+ commonService.BUSINESS_SERVICE + commonService.BUSINESS_SERVICE_GET + '/' + btoa(JSON.stringify(vendorid)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("Business Details  ", data);
            deferred.resolve(data);
        }).error(function (data, status) {
            console.error('Error while fetching Users    ', data);
            deferred.reject(data);
        });        
        return deferred.promise;
    }
    
    function uploadFile (element, type){
        var formData = new FormData();
            var file = element.files[0];
            var cookiesDetail = $cookies.getObject('userInfoCookies')
            formData.append("file", file);
            formData.append("typename", 'business');
            formData.append("type", type);
            formData.append("vendorid", cookiesDetail.jti);

            var deferred = $q.defer();
            $http({
                    method: 'POST',
                    url: constantsService.url + commonService.BUSINESS_SERVICE +'/newDocument',
                    headers: { 'Content-Type': undefined}, 
                    transformRequest: angular.identity, 
                    transformResponse: angular.identity,
                    data: formData
                }).success(function (data) {
                    console.log("sucesss");
                    deferred.resolve(data);
                }).error(function (data) {
                    console.error('Error while fetching Users    ');
                    deferred.reject(data);
                });
                return deferred.promise;
    };


    function getImage(type){
        var requestParam = {
            name: type
        }
        console.log("type",type);
        console.log(constantsService.url+ commonService.BUSINESS_SERVICE + '/getimage' );
         var deferred = $q.defer();
       $http({
            method : commonService.METHOD_GET,
            url :  constantsService.url+ commonService.BUSINESS_SERVICE + '/getimage' + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            deferred.resolve(data);
        }).error(function (data, status) {
            console.error('Error while fetching Users    ', data);
            deferred.reject(data);
        });
        return deferred.promise;
    }
    
   
    }