angular
.module('app')
.factory('newspaperService', newspaperService);

newspaperService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function newspaperService(constantsService, $q, $http, commonService) {
	 var service = {
        getNewspaperDetail:getNewspaperDetail,
        addNewspaper:addNewspaper
    };
    return service;

    function getNewspaperDetail(parameters){
    	console.log(parameters.offset)
        var requestParam = {
			offset: parameters.offset == undefined ? '':parameters.offset,
                   limit:'',
                filters:{
                    categories: (parameters.categories.length > 0 && parameters.categories != undefined) ? parameters.categories : [],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    frequencies:[],
                    publications:(parameters.publications.length > 0 && parameters.publications != undefined)  ? parameters.publications : []
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
    	
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url + commonService.NEWSPAPER + commonService.GET_NEWSPAPER + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log(data);
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }

    function addNewspaper(requestParam){
        console.log('addNewspaper service  '+JSON.stringify(requestParam));
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.NEWSPAPER + commonService.ADD_NEWSPAPER + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER "+data);          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;

    }


}