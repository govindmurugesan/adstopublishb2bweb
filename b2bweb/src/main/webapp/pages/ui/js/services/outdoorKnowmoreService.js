angular.module('app').factory('outdoorKnowmoreService', outdoorKnowmoreService);

outdoorKnowmoreService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function outdoorKnowmoreService(constantsService, $q, $http, commonService) {
		var service = {
	        updateOutdoor: updateOutdoor
	    };
	    return service;


	    function updateOutdoor(requestParam){
		 	var deferred = $q.defer();
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.OUTDOOR + commonService.ADD_OUTDOOR + '/' + btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity,
	            headers: {  'Content-Type': undefined}
	        }).success(function (data, status, headers, config) {  
	            console.log("response in server");          
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            deferred.reject(data);
	        });        
	        return deferred.promise;

    	}
}