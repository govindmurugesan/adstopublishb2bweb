(function() {
'use strict';


	var constantsService_prod = {
		url: 'http://portfolio.saptalabs.com:8080/b2bwebapi',
		facebookAppId : '57578686',
		googleId: '263423522351-' 
	};

	var constantsService_dev = {
		url: 'http://192.168.2.13:8080/b2bweb',
		staticSource : 'pages',
		facebookAppId : '57578686',
		googleId: '263423522351-'

	};

	angular.module("app").constant('constantsService', constantsService_dev);

	})();
