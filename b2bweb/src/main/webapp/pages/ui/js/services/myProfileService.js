angular.module('app').factory('myProfileService', myProfileService);

myProfileService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function myProfileService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addbankdetail: addbankdetail,
        getbankdetail: getbankdetail,
        uploadFile: uploadFile
    };

    return service;