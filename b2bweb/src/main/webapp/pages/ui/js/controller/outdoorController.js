angular
    .module('app')
    .controller('outdoorController', outdoorController);

    outdoorController.$inject = ['$scope', 'outdoorService', '$rootScope', '$location'];

    function outdoorController($scope, outdoorService, $rootScope, $location) {
        var outdoorControllerScope = this;
        outdoorControllerScope.loader = true;
        outdoorControllerScope.getOutdoor = getOutdoor;
        outdoorControllerScope.knowMore = knowMore;
        outdoorControllerScope.getFilterLocation = getFilterLocation;
        outdoorControllerScope.getFilterMediatype = getFilterMediatype;
        outdoorControllerScope.getFilterSize = getFilterSize;
        outdoorControllerScope.sortBy = sortBy;
        outdoorControllerScope.addNewOutdoor = addNewOutdoor;
        outdoorControllerScope.addMore = addMore;
        //addnew
        outdoorControllerScope.addOutdoor = addOutdoor;
        outdoorControllerScope.place = [];
        outdoorControllerScope.mediatype = [];
        outdoorControllerScope.sizee = [];



        var outdoorList = [];
        var dataLength = 0;
        
        var sortBy = 'topsearch';
        $scope.regularOptions = {
            mediaOptions:[
                {
                    area: "",
                    cardRate: "",
                    discountedRate: "",
                    imageUrl: "",
                    leadTime: "",
                    printingAndMountingRate: "",
                    ratePerSquareFeet: "",
                    vendorRate: ""
                }
            ]
        };

            getOutdoor();

            $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
            $scope.locations = ['Bangalore', 'Mumbai', 'Hyderabad', 'Chandigarh'];
            $scope.mediatypes = ['Backlit Wall', 'Bus Shelter', 'Designer Wall', 'Fuel Pump Hoarding'];
            $scope.sizes = ['Large', 'Medium', 'Small'];
            $scope.lits = ['BACK LIT', 'FRONT LIT', 'NON LIT'];
            function knowMore(record){                
                $location.url('/outdoorKnowmore');
                recordDetails = record;
            }

            
             function sortBy(arg){  
                console.log("sort by", arg);
                dataLength = 0;  
                outdoorList = []; 
                if (arg == '') {
                    sortBy = 'topsearch';
                } else {
                   sortBy = arg;
                }
                getOutdoor();  
            }

            function getFilterLocation(arg){  
                dataLength = 0;  
                outdoorList = []; 
                if (outdoorControllerScope.place.indexOf(arg) === -1) {
                    outdoorControllerScope.place.push(arg);
                } else {
                    outdoorControllerScope.place.splice(outdoorControllerScope.place.indexOf(arg), 1);
                }
                getOutdoor();       
            }

            function getFilterMediatype(arg){
                dataLength = 0;  
                outdoorList = [];   
                if (outdoorControllerScope.mediatype.indexOf(arg) === -1) {
                    outdoorControllerScope.mediatype.push(arg);
                } else {
                    outdoorControllerScope.mediatype.splice(outdoorControllerScope.mediatype.indexOf(arg), 1);
                } 
                getOutdoor();       
            }

            function getFilterSize(arg){
                dataLength = 0;  
                outdoorList = [];   
                if (outdoorControllerScope.sizee.indexOf(arg) === -1) {
                    outdoorControllerScope.sizee.push(arg);
                } else {
                    outdoorControllerScope.sizee.splice(outdoorControllerScope.sizee.indexOf(arg), 1);
                } 
                getOutdoor();       
            }

            //get Outdoor  details
            function getOutdoor(){
                outdoorControllerScope.loader = true;
               var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    mediaType: (outdoorControllerScope.mediatype.length > 0 && outdoorControllerScope.mediatype.length != undefined ? outdoorControllerScope.mediatype : []),
                    sizee: (outdoorControllerScope.sizee.length > 0 && outdoorControllerScope.sizee.length != undefined ? outdoorControllerScope.sizee : []),
                    geographies: (outdoorControllerScope.place.length > 0 && outdoorControllerScope.place.length != undefined ? outdoorControllerScope.place : [])
                };

                outdoorService.getOutdoorDetail(filterPar).then(function(data){
                    var jsonFormatedData = JSON.parse(data);
                    if(jsonFormatedData.response.success == 'true'){
                        outdoorControllerScope.outdoorDetail = jsonFormatedData.response.results;
                        for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                            outdoorList.push(JSON.parse(outdoorControllerScope.outdoorDetail)[i]);
                            }
                        outdoorList.push(JSON.parse(outdoorControllerScope.outdoorDetail));
                        dataLength += JSON.parse(jsonFormatedData.response.results).length;
                        $scope.count = jsonFormatedData.response.count;
                        outdoorControllerScope.outdoorDetail = outdoorList;
                        outdoorControllerScope.count = dataLength;
                        //console.log('dataLength  ', outdoorList)
                        outdoorControllerScope.loader = false;
                    }

                },function(error){
                        console.log("error",error)
                });
            }

            // add outdoor details
            function addNewOutdoor(){
                $location.url('/outdoorNew');
            }

            $scope.currentTab = 'outdoorGridView';

            $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
            $scope.options=['mediaOption1'];

            function addMore(record){                                
                var count = 1 + $scope.options.length;                     
                var val = $scope.options[count-2]; 
                 console.log("2222",record.mediaOptions.regularOptions);                    
               if(record.mediaOptions != undefined && record.mediaOptions.regularOptions != undefined && record.mediaOptions.regularOptions[val]) {
                    $scope.options.push('mediaOption'+count);
                     console.log('2222',record.mediaOptions.regularOptions[val]);      
               }                                                            
            }

            var id = '';
            function addOutdoor(record){
               record.geography = [];
               record.urlSlug = '';
               record.uniqueId = '';  
               record._id = id; 
                outdoorService.addOutdoor(record).then(function(data){
                     var jsonFormatedData = JSON.parse(data);                        
                    if(jsonFormatedData.response.success == 'true'){                           
                       var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                       console.log(respDate);
                       id = respDate._id['$oid'];
                    }
                },function(error){
                    console.log("error",error)
                });        
                
            }


    }

        

