angular.module('app').controller('newspaperKnowmoreController', newspaperKnowmoreController);

    newspaperKnowmoreController.$inject = ['$scope', 'newspaperKnowmoreService', '$rootScope', '$location'];

    function newspaperKnowmoreController($scope, newspaperKnowmoreService, $rootScope, $location) {
    	var newspaperKnowmoreControllerScope = this;
    	newspaperKnowmoreControllerScope.updateNewspaper = updateNewspaper;
        newspaperKnowmoreControllerScope.getNewspaperKnowmoreDetail = getNewspaperKnowmoreDetail;
    	newspaperKnowmoreControllerScope.recordDetails = recordDetails;
        newspaperKnowmoreControllerScope.show = 0;
        newspaperKnowmoreControllerScope.today = today;
        newspaperKnowmoreControllerScope.clear = clear;
        newspaperKnowmoreControllerScope.open1 = open1;
        newspaperKnowmoreControllerScope.currentPage = recordDetails.name;

    	function updateNewspaper(){
    		newspaperKnowmoreControllerScope.recordDetails.categoryId=[];
    		newspaperKnowmoreControllerScope.recordDetails.geography=[];

    		console.log("know more newspaper");
    		newspaperKnowmoreService.addNewspaper(newspaperKnowmoreControllerScope.recordDetails).then(function(data){
                 var jsonFormatedData = JSON.parse(data);                        
                if(jsonFormatedData.response.success == 'true'){    
                   var respDate = JSON.parse(jsonFormatedData.response.results);
                   console.log(respDate);                   
                }
            },function(error){
                console.log("error",error)
            }); 
    	}
        $scope.records = [
            {"name":"Matrimonial"},
            {"name":"Recruitments"},
            {"name":"Property For Sale"},
            {"name":"Property For Rent"},
            {"name":"Name Change"},
            {"name":"Lost Found"},
            {"name":"Vehicles"},
            {"name":"Astrology"},
            {"name":"Business"},
            {"name":"Computers"},
            {"name":"Classified Remebererance"},
            {"name":"Education"},
            {"name":"Obituary"},
            {"name":"Personal Announcement"},
            {"name":"Personal Messages"},
            {"name":"Retail"},
            {"name":"Serices"},
            {"name":"Travel"}

        ];
        $scope.tabs = [{
                title: 'DISPLAY Ad',
                url: 'DISPLAYAd'
        }, {
                title: 'Classified DISPLAY Ad',
                url: 'ClassifiedDISPLAYAd'
        },
        {
                title: 'Classified Text Ad',
                url: 'ClassifiedTextAd'
        }];

        $scope.currentTab = 'DISPLAYAd';

        $scope.onClickTab = function (tab) {
            $scope.currentTab = tab.url;
            dataLength = 0 ;
            newspaperList = [];
            newspaperKnowmoreControllerScope.newspaperDetails = [];
                             
        }
        
        $scope.isActiveTab = function(tabUrl) {
            return tabUrl == $scope.currentTab;
        }
   
          function today() {
            $scope.dt = new Date();
          };
        today();

         function clear() {
            $scope.dt = null;
          };

          $scope.dateOptions = {

            formatYear: 'yy',
            maxDate: new Date(3030, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };

          function open1() {
            $scope.popup1.opened = true;
         
          };
          $scope.popup1 = {
            opened: false
          };

           console.log($scope.dt);
           function getNewspaperKnowmoreDetail(){
            var  filterPar = {
                    offset:0,
                   limit:'',
                filters:{
                    categories:[],
                    geographies: [],
                    languages: [],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[]
                },
                sortBy: '',
                recommended:''
                    
            };
            newspaperKnowmoreService.getNewspaperKnowmoreDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    console.log(dataLength);
                    newspaperKnowmoreControllerScope.newspaperKnowmoreDetails = jsonFormatedData.response.results;
                    newspaperList.push(JSON.parse(newspaperKnowmoreControllerScope.newspaperKnowmoreDetails));    
                    $scope.count = jsonFormatedData.response.count;                    
                    //console.log(newspaperList);
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    newspaperKnowmoreControllerScope.newspaperKnowmoreDetails = newspaperList;
                    $rootScope.spinner = false;

                }

            })
        }
    }


