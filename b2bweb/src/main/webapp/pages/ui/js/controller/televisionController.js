angular
  .module('app')
  .controller('televisionController', televisionController);

  televisionController.$inject = ['$scope', 'televisionService', '$rootScope', '$location'];

  function televisionController($scope, televisionService, $rootScope, $location) {
    var televisionControllerScope = this;
        televisionControllerScope.loader = true;
        televisionControllerScope.getTelevision = getTelevision;
        televisionControllerScope.knowMore = knowMore;
        televisionControllerScope.getFilterLocation = getFilterLocation;
        televisionControllerScope.getFilterLang = getFilterLang;
        televisionControllerScope.getChannelgenre = getChannelgenre;
        televisionControllerScope.sortBy = sortBy;
        televisionControllerScope.addTelevision = addTelevision;
        televisionControllerScope.addNewTelevision = addNewTelevision;
        televisionControllerScope.addMore = addMore;
        televisionControllerScope.place = [];
        televisionControllerScope.lang = [];
        televisionControllerScope.channelgenre = [];

        var televisionList = [];
        var dataLength = 0;
        
        var sortBy = 'topsearch';

        getTelevision();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.channelgenres = [' Comedy ', ' Cookery ', 'Entertainment ', 'Fashion And Lifestyle '];
        
        function knowMore(record){
            $location.url('/televisionKnowmore');
            recordDetails= record;
        }

        
         function sortBy(arg){  
            console.log("sort by", arg);
            dataLength = 0;  
            televisionList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getTelevision();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            televisionList = []; 
            if (televisionControllerScope.place.indexOf(arg) === -1) {
                televisionControllerScope.place.push(arg);
            } else {
                televisionControllerScope.place.splice(televisionControllerScope.place.indexOf(arg), 1);
            }
            getTelevision();       
        }

        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];

        function getFilterLang(arg){
            dataLength = 0;  
            televisionList = [];   
            if (televisionControllerScope.lang.indexOf(arg) === -1) {
                televisionControllerScope.lang.push(arg);
            } else {
                televisionControllerScope.lang.splice(televisionControllerScope.lang.indexOf(arg), 1);
            } 
            getTelevision();       
        }

        function getChannelgenre(arg){
            dataLength = 0;  
            televisionList = [];   
            if (televisionControllerScope.channelgenre.indexOf(arg) === -1) {
                televisionControllerScope.channelgenre.push(arg);
            } else {
                televisionControllerScope.channelgenre.splice(televisionControllerScope.channelgenre.indexOf(arg), 1);
            } 
            getTelevision();       
        }

        //get television details
        function getTelevision(){
            console.log("hi");
           televisionControllerScope.loader = true;
           var  filterPar = {
                offset: dataLength,
                sortBy: sortBy,
                languages: (televisionControllerScope.lang.length > 0 && televisionControllerScope.lang.length != undefined ? televisionControllerScope.lang : []),
                geographies: (televisionControllerScope.place.length > 0 && televisionControllerScope.place.length != undefined ? televisionControllerScope.place : []),
                channelgenre: (televisionControllerScope.channelgenre.length > 0 && televisionControllerScope.channelgenre.length != undefined ? televisionControllerScope.channelgenre : []),
            };

            televisionService.getTelevisionDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    televisionControllerScope.televisionDetails = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                         televisionList.push(JSON.parse(televisionControllerScope.televisionDetails)[i]); 
                      }
                    console.log(televisionList);
                    $scope.count = jsonFormatedData.response.count;
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    televisionControllerScope.televisionDetails = televisionList;
                    televisionControllerScope.count = dataLength;
                    televisionControllerScope.loader = false;
                }

            },function(error){
                    console.log("error",error)
            })
        }
            
        $scope.currentTab = 'televisionGridView';

        // $scope.tabs = [{
        //     title: 'Grid View',
        //     url: 'televisionGridView'
        // }, {
        //     title: 'List View',
        //     url: 'televisionListView'
        // }];

        // $scope.currentTab = 'televisionGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     televisionList = [];
        //     televisionControllerScope.televisionDetails = [];
        //     getTelevision();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }


        function addMore(optionType){
            var count = 1 + televisionControllerScope[optionType].length;
            televisionControllerScope[optionType].push('mediaOption'+count);
        }

        televisionControllerScope.mediaOptions = ['regularOptions','planningOptions'];
        televisionControllerScope.regularOptions = ['mediaOption1'];
        televisionControllerScope.planningOptions = ['allDayPlan']
        function addNewTelevision(){
            $location.url('/televisionNew');
        }

        var id= '';
        function addTelevision(){
             console.log("chethannnn   "+ televisionControllerScope.television);
            televisionControllerScope.television.categoryId = [];
            televisionControllerScope.television.geography = [];
            televisionControllerScope.television._id = id;

            console.log("chethannnn   ", televisionControllerScope.television);
            televisionService.addTelevision(televisionControllerScope.television).then(function(data){                
                var jsonFormatedData = JSON.parse(data);  
                console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                       
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$id'];
                }
            },function(error){
                console.log("error",error)
            });  
        }
    
  }