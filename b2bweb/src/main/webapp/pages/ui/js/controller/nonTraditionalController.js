angular
    .module('app')
    .controller('nonTraditionalController', nonTraditionalController);

    nonTraditionalController.$inject = ['$scope', 'nonTraditionalService', '$rootScope', '$location'];

    function nonTraditionalController($scope, nonTraditionalService, $rootScope, $location) {
        var nonTraditionalControllerScope = this;
        nonTraditionalControllerScope.loader = true;
        nonTraditionalControllerScope.getNonTraditional = getNonTraditional;
        nonTraditionalControllerScope.knowMore = knowMore;
        nonTraditionalControllerScope.getFilterLocation = getFilterLocation;
        nonTraditionalControllerScope.getFilterCategory = getFilterCategory;
        nonTraditionalControllerScope.sortBy = sortBy;
        nonTraditionalControllerScope.addNewNonTraditional = addNewNonTraditional;
        nonTraditionalControllerScope.addNonTraditional = addNonTraditional;
        nonTraditionalControllerScope.addMore = addMore;
        nonTraditionalControllerScope.place = [];
        nonTraditionalControllerScope.categories = [];

        var nonTraditionalList = [];
        var dataLength = 0;
        var sortBy = 'topsearch';

            getNonTraditional();

            $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
            $scope.categories = [' Food And Hospitality', 'Live Screens ', 'Others ', ' Print '];
            $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];

            function knowMore(record){
                $location.url('/nontraditionalKnowmore');
                recordDetails = record;
            }
            
            function sortBy(arg){  
                console.log("sort by", arg);
                dataLength = 0;  
                nonTraditionalList = []; 
                if (arg == '') {
                    sortBy = 'topsearch';
                } else {
                   sortBy = arg;
                }
                getNonTraditional();  
            }

            function getFilterLocation(arg){  
                dataLength = 0;  
                nonTraditionalList = []; 
                if (nonTraditionalControllerScope.place.indexOf(arg) === -1) {
                    nonTraditionalControllerScope.place.push(arg);
                } else {
                    nonTraditionalControllerScope.place.splice(nonTraditionalControllerScope.place.indexOf(arg), 1);
                }
                getNonTraditional();       
            }

            function getFilterCategory(arg){
                dataLength = 0;  
                nonTraditionalList = [];   
                if (nonTraditionalControllerScope.categories.indexOf(arg) === -1) {
                    nonTraditionalControllerScope.categories.push(arg);
                } else {
                   nonTraditionalControllerScope.categories.splice(nonTraditionalControllerScope.categories.indexOf(arg), 1);
                } 
                getNonTraditional();       
            }

            //get nonTraditional  details
            function getNonTraditional(){
                nonTraditionalControllerScope.loader = true;
               var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    categories: (nonTraditionalControllerScope.categories.length > 0 && nonTraditionalControllerScope.categories.length != undefined ? nonTraditionalControllerScope.categories : []),
                    geographies: (nonTraditionalControllerScope.place.length > 0 && nonTraditionalControllerScope.place.length != undefined ? nonTraditionalControllerScope.place : [])
                };
                
                nonTraditionalService.getNonTraditionalDetail(filterPar).then(function(data){
                    var jsonFormatedData = JSON.parse(data);
                    if(jsonFormatedData.response.success == 'true'){
                        nonTraditionalControllerScope.nonTraditionalDetail = jsonFormatedData.response.results;
                        for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                            nonTraditionalList.push(JSON.parse(nonTraditionalControllerScope.nonTraditionalDetail)[i]);
                            }
                         $scope.count = jsonFormatedData.response.count;
                        dataLength += JSON.parse(jsonFormatedData.response.results).length;
                        nonTraditionalControllerScope.nonTraditionalDetail = nonTraditionalList;
                        console.log('dataLength  ', dataLength)
                        nonTraditionalControllerScope.count = dataLength;
                        nonTraditionalControllerScope.loader = false;
                    }

                },function(error){
                        console.log("error",error)
                })
            }

            function addMore(optionType){
                var count =  nonTraditionalControllerScope.regularOptions.length + 1;
                nonTraditionalControllerScope.regularOptions.push("mediaOption"+count);
            }
                
            nonTraditionalControllerScope.mediaOptions = ['regularOptions'];
            nonTraditionalControllerScope.regularOptions = ["mediaOption1"];
            function addNewNonTraditional(){ 
                 console.log("in  ", nonTraditionalControllerScope.options);               
                 $location.url('/nonTraditionalNew');
            }

            var id= '';
            function addNonTraditional(){                 
                nonTraditionalControllerScope.nonTraditionl._id = id; 
                console.log("chethannnn   ", nonTraditionalControllerScope.nonTraditionl);
                nonTraditionalService.addnonTraditional(nonTraditionalControllerScope.nonTraditionl).then(function(data){                    
                     var jsonFormatedData = JSON.parse(data);  
                     console.log('jsonFormatedData ',jsonFormatedData);                      
                    if(jsonFormatedData.response.success == 'true'){                           
                       var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                       console.log('respDats', respDate);
                       id = respDate._id['$id'];
                    }
                },function(error){
                    console.log("error",error)
                });  
            }

            $scope.currentTab = 'nonTraditionalGridView';


            // $scope.tabs = [{
            //         title: 'Grid View',
            //         url: 'nonTraditionalGridView'
            // }, {
            //         title: 'List View',
            //         url: 'nonTraditionalListView'
            // }];

            // $scope.currentTab = 'nonTraditionalGridView';

            // $scope.onClickTab = function (tab) {
            //     $scope.currentTab = tab.url;
            //     dataLength = 0 ;
            //     nonTraditionalList = [];
            //     nonTraditionalControllerScope.nonTraditionalDetail = [];
            //     getNonTraditional();                     
            // }
            
            // $scope.isActiveTab = function(tabUrl) {
            //     return tabUrl == $scope.currentTab;
            // }
        
    }

        

