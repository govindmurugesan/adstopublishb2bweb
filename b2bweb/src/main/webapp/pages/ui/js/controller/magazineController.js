 angular.module('app').controller('magazineController', magazineController);


	magazineController.$inject = ['$scope', 'magazineService', '$location', '$localStorage',  '$rootScope'];

	function magazineController($scope, magazineService, $location, $localStorage, $rootScope) {
		var magazineControllerScope = this;
    magazineControllerScope.loader = true;
		magazineControllerScope.getMagazine = getMagazine;
		magazineControllerScope.dropboxitemselected = dropboxitemselected;
		magazineControllerScope.knowMore = knowMore;
    magazineControllerScope.sortBy = sortBy;
		magazineControllerScope.addNewMagazine = addNewMagazine;
		magazineControllerScope.addMagazine = addMagazine;
    magazineControllerScope.getFilterLocation = getFilterLocation;
    magazineControllerScope.getFilterLang = getFilterLang;
    magazineControllerScope.getFilterCategory = getFilterCategory;
    magazineControllerScope.category = [];
    magazineControllerScope.targetgroup = [];
    magazineControllerScope.frequency = [];
    magazineControllerScope.place = [];
    magazineControllerScope.lang = [];

		var dataLength = 0 ;		
		var magzinList =[];	
		
    var sortBy = 'circulation';	
		function knowMore(record){
			$location.url('/knowMore');
			recordDetails= record;
		}
    
    getMagazine();

    $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
    $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
    $scope.categories = ['Agriculture and Veterinary', 'Animation', 'Armed Force', 'Art And Culture'];
    $scope.targetgroups = ['Adult', 'B2B', 'B2C', 'Female'];

    function knowMore(record){
        $location.url('/magazineKnowMore');
        recordDetails = record;
    }

    function sortBy(arg){  
        console.log("sort by", arg);
        dataLength = 0;  
        newspaperList = []; 
        if (arg == '') {
            sortBy = 'circulation';
        } else {
           sortBy = arg;
        }
        getMagazine();  
    }
    function getFilterLocation(arg){  
    	dataLength = 0;    
    	magzinList =[];      
        if (magazineControllerScope.place.indexOf(arg) === -1) {
            magazineControllerScope.place.push(arg);
        } else {
            magazineControllerScope.place.splice(magazineControllerScope.place.indexOf(arg), 1);
        }
        getMagazine();       
    }

    function getFilterLang(arg){
    	dataLength = 0;
    	magzinList =[];
        if (magazineControllerScope.lang.indexOf(arg) === -1) {
            magazineControllerScope.lang.push(arg);
        } else {
            magazineControllerScope.lang.splice(magazineControllerScope.lang.indexOf(arg), 1);
        }
        getMagazine();       
    }

    function getFilterTargetgroup(arg){
        dataLength = 0;
        magzinList =[];
        if(magazineControllerScope.targetgroup.indexOf(arg) === -1){
            magazineControllerScope.targetgroup.push(arg);
        }else{
            magazineControllerScope.targetgroup.splice(magazineControllerScope.targetgroup.indexOf(arg), 1);
        }
        getMagazine();       
    }

    function getFilterFrequence(arg){
        dataLength = 0;
        magzinList =[];
        if(magazineControllerScope.frequency.indexOf(arg) === -1){
            magazineControllerScope.frequency.push(arg);
        }else{
            magazineControllerScope.frequency.splice(magazineControllerScope.frequency.indexOf(arg), 1);
        }
        getMagazine();       
    }

    function getFilterCategory(arg){
        dataLength = 0;
        magzinList =[];
        if(magazineControllerScope.category.indexOf(arg) === -1) {
            magazineControllerScope.category.push(arg);
        }else{
            magazineControllerScope.category.splice(magazineControllerScope.category.indexOf(arg), 1);
        }
        getMagazine(); 
    }

		function getMagazine(){
			magazineControllerScope.loader = true;
			var parameters = {
				offset: dataLength,
				languages: (magazineControllerScope.lang.length > 0 && magazineControllerScope.lang.length != undefined ? magazineControllerScope.lang : []),
        geographies: (magazineControllerScope.place.length > 0 && magazineControllerScope.place.length != undefined ? magazineControllerScope.place : []),
        categoryName: (magazineControllerScope.category.length > 0 && magazineControllerScope.category.length != undefined ? magazineControllerScope.category : []),
        targetgroup: (magazineControllerScope.targetgroup.length > 0 && magazineControllerScope.targetgroup.length != undefined ? magazineControllerScope.targetgroup : []),
        frequency: (magazineControllerScope.frequency.length > 0 && magazineControllerScope.frequency.length != undefined ? magazineControllerScope.frequency : [])
      }
			magazineService.getMagazine(parameters).then(function(data){			
				var results = JSON.parse(data);
				if(results.response.success == 'true'){
          for(var i = 0; i < JSON.parse(results.response.results).length; i++){
             magzinList.push(JSON.parse(results.response.results)[i]);
          }
					$scope.count = results.response.count;  
					dataLength += JSON.parse(results.response.results).length;
					$scope.records = magzinList;
          magazineControllerScope.count = dataLength;
					magazineControllerScope.loader = false;
				}					

			});
     

		}

		function addMagazine(magazineDetails){
			console.log("getMagazine start");
			$rootScope.spinner = false;
			magazineService.addMagazine().then(function(data){				
				var results = JSON.parse(data);
				console.log("results", results.response.success);
				if(results.response.success == 'true'){
					$scope.records = results.response.results;	
				}
				$rootScope.spinner=false;
			});
		}

		function UpdateMagazine(magazineDetails){
			console.log("updateMagazine start");
			$rootScope.spinner = false;
			magazineService.updateMagazine().then(function(data){				
				var results = JSON.parse(data);
				console.log("results", results.response.success);
				if(results.response.success == 'true'){
					$scope.records = results.response.results;	
				}
				$rootScope.spinner=false;
			});
		}
		function dropboxitemselected(location){			
			dataLength = 0;
		}

		
		magazineControllerScope.mediaOptions = ['regularOptions'];
        magazineControllerScope.regularOptions = ["fullPage", "halfPage", "insideFrontCover", "insideBackCover", "backCover", "doubleSpread", "halfPageVertical", "halfPageHorizontal", "verticalStrip", "horizontalStrip", "frontGatefold", "centerDoubleSpread"];
        function addNewMagazine(){
            $location.url('/magazineNew');
        }

        var id= '';
        function addMagazine(){
        	  magazineControllerScope.magazine.categoryId = [];
            magazineControllerScope.magazine.geography = [];  
            magazineControllerScope.magazine.keywords = [];           
            magazineControllerScope.magazine.uniqueId = '';  
            magazineControllerScope.magazine.thumbnail = ''; 
            magazineControllerScope.magazine.views = '1'; 
            magazineControllerScope.magazine._id = id; 
            magazineService.addMagazine(magazineControllerScope.magazine).then(function(data){
                
                 var jsonFormatedData = JSON.parse(data);                  
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   id = respDate._id['$oid'];
                }
            },function(error){
                console.log("error",error)
            });  
        }
        

        $scope.currentTab = 'radioGridView';


		// $scope.tabs = [{
  //           title: 'Grid View',
  //           url: 'radioGridView',

  //       }, {
  //           title: 'List View',
  //           url: 'radioListView'
  //       }];
        
  //       $scope.currentTab = 'radioGridView';
  //       $scope.onClickTab = function (tab) {
  //           $scope.currentTab = tab.url;
  //           dataLength = 0 ;
  //           magzinList = []; 
  //           $scope.records = [];
  //           console.log(magzinList + '   =   ' +$scope.records);
  //           getMagazine();			          
  //       }        
  //       $scope.isActiveTab = function(tabUrl) {
  //           return tabUrl == $scope.currentTab;
  //       }

	}