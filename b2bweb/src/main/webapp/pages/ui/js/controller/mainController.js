angular
	.module('app')
	.controller('mainController', mainController);

	mainController.$inject = ['$scope', 'constantsService', 'homeService', '$localStorage', '$location', '$rootScope','$window','$cookies'];

	function mainController($scope, constantsService, homeService, $localStorage, $location, $rootScope,$window,$cookies) {
		var vm = this;
		vm.staticSource = constantsService.staticSource;
		vm.login = login;
		vm.register = register;
		vm.forgetpass = forgetpass;
		vm.close = close;
		vm.newregister = newregister;
		vm.show = show;
		
		$localStorage.jwtToken = [];

		vm.loginErrorMsg = false;
		vm.divShow = "login";

		//close method
		function close(){
			$scope.vm.divShow = "";
			$scope.vm.loginUser = {};
			$scope.vm.loginErrorMsg = false;
			$('#loginDescp').modal('hide');
			$('.modal-backdrop').remove();

		}

		 function show (arg) {
			vm.loginUser = {};
			vm.userRegistrationDetail = {};
			vm.divShow = arg;

			if(arg == 'signup'){
				$('#loginDescp').modal('show');
				
			}
			if(arg == 'login'){
				$('#loginDescp').modal('show');
			}
		
        }



		//login method
		function login(){
			console.log("vm.loginUser",vm.loginUser);
			if(vm.loginUser){
				if(vm.loginUser.username != undefined && vm.loginUser.password != undefined){
					homeService.login(vm.loginUser).then(
			         	function(data) {
							var results = JSON.parse(data);		
							console.log("results",results);
							var success = results.response.success;
							if(success == 'true'){

								var base64Url = $localStorage.token.split('.')[1];
								$rootScope.spinner=false;
								var base64 = base64Url.replace('-', '+').replace('_', '/');
								$localStorage.jwtToken = JSON.parse(atob(base64));
								/*$rootScope.usrName = $localStorage.jwtToken.sub*/
							
								var expireDate = new Date();
		  						expireDate.setDate(expireDate.getDate() + 7);
		  
								 $cookies.putObject('userInfoCookies', $localStorage.jwtToken, {'expires': expireDate});
								 var userDetailsInCookies = $cookies.getObject('userInfoCookies');
								 console.log("cookiess",userDetailsInCookies);
								 $('#loginDescp').modal('hide');
								$('.modal-backdrop').remove();
								
		           				 $rootScope.$emit("CallParentMethod", {});
		        

								$rootScope.usrId = $localStorage.jwtToken.jti;
								$location.url('/radio');
							}else{
								
								vm.loginErrorMsg = true;
							}
							
			        },
			        function(errResponse){
			        	vm.loginErrorMsg = true;
			             console.error('Error while fetching Users');
			        });
				}else{
					vm.loginErrorMsg = true;
				}

			}else{
				vm.loginErrorMsg = true;
			}

		}


		function register(user){			
					console.log("user", user);
					if(user != undefined){
					
						homeService.register(user).then(function(registerServiceResponsedata) {		 
						 	
						 	var results = JSON.parse(registerServiceResponsedata);	
						 	console.log("datadata",results.response.errors);					
							 	
							if(results.response.success == 'true'){
							 	if(results.response.results == 'register'){
			                        console.log("new registerd");
			                         show('signup',user)
			                         $rootScope.spinner = false;
			                    }else{
			                        $('#loginDescp').modal('hide');
			                        console.log("true")
			                        $localStorage.jwtToken="";
			                        user.name="";
			                        user.email="";
			                        user.mobileNo="";
			                        user.password="";
			                        user.confirmPassword="";
			                    
			                        var base64Url = $localStorage.token.split('.')[1];
			                        
			                        var base64 = base64Url.replace('-', '+').replace('_', '/');
			                        $localStorage.jwtToken = JSON.parse(atob(base64));
			                        console.log($localStorage.jwtToken.sub);
			                        $rootScope.usrName = $localStorage.jwtToken.sub
			                        console.log($localStorage.jwtToken)
			                        $rootScope.usrId = $localStorage.jwtToken.jti;
			                        $rootScope.spinner=false;
			                        $location.url('/index');
			                    }	
							}else{
								console.log("false");
						 		if(results.response.errors == 'already registerd'){
						 			console.log("registerd");
						 			show('login','');
						 			$rootScope.spinner = false;
						 		}
						 	}
							 
						 },
						 function(errResponse){
							 console.error('Error while fetching Users');
						 });	
					}else{

					}
					}

			
		

		//forgot password method
		function forgetpass(user){
			$rootScope.spinner = true;
			console.log("My forgetpass Method called ...");
			homeService.forgetpass(user).then(function(data) {
	        	  	console.log("aaa", data);
	        	  	var results = JSON.parse(data);		
					var success = results.response.success;
					if(success == 'true'){
						
						//TODO modal should be closed here
						$location.url('/resetPassword');
					}else{
						// $rootScope.spinner = false;
						$location.url('/error');
					}
	          },
	          function(errResponse){
	              console.error('Error while fetching Users');
	          });
			$rootScope.spinner = false;
		}


		function  newregister() {
			console.log("srf");
		}
		
	}

