angular
	.module('app')
	.controller('bankDetailsController', bankDetailsController);

	bankDetailsController.$inject = ['$scope', 'bankDetailsService', '$location', '$localStorage', '$rootScope', '$cookies'];

	function bankDetailsController($scope, bankDetailsService, $location, $localStorage, $rootScope, $cookies) {
		var bankDetailsControllerScope = this;
		getbankdetail();
		bankDetailsControllerScope.addbankdetail = addbankdetail;
		bankDetailsControllerScope.uploadFile = uploadFile;
		bankDetailsControllerScope.getImage = getImage;
		bankDetailsControllerScope.bankDetails = recordDetails;
		bankDetailsControllerScope.decline = decline;

		function getImage(type){
			$rootScope.spinner=true;
			bankDetailsService.getImage(type).then(	function(data) {
				var res = JSON.parse(data);
				var typename = type.split('_')[2];
				console.log('res', res.image);


				if (typename == "addressproofurl")
					$scope.addressproofurl = res.image;
				else if(typename == "cancelledchequeurl")
					$scope.cancelledchequeurl = res.image;
					
				$rootScope.spinner=false;		
				},
				function(errResponse){
					$rootScope.spinner=false;
					console.log('errResponse  ===  ',errResponse);
				}
			);	
		}

		$scope.uploadedFile = function(element, type) {
			bankDetailsService.uploadFile(element, type).then(
				function(data) {
					$rootScope.spinner=false;
					console.log(data);			
				},
				function(errResponse){
					$rootScope.spinner=false;
					console.log('errResponse  ===  ',errResponse);
				}
			);	
		}	


		function addbankdetail(bankDetails){
			bankDetailsService.addbankdetail(bankDetails).then(function(data){
				var results = data;
       			console.log(data);
			});
		}
		
		var recordDetails = null;
		function getbankdetail(){
			$scope.vm.spinner = true;
			var cookiesDetail = $cookies.getObject('userInfoCookies');
      	console.log("cookiesDetail",cookiesDetail.jti);
      		var requestParam = {
            	vendorid : cookiesDetail.jti
        	}
		bankDetailsService.getbankdetail(requestParam).then(function(data){
        var jsonFormatedData = JSON.parse(data);
       		 console.log("jsonFormatedData", jsonFormatedData);
         if(jsonFormatedData.response.success == 'true'){
             var bankDetails = JSON.parse(jsonFormatedData.response.results);
             bankDetailsControllerScope.bankDetails = bankDetails[0];
             recorddetails = data;
             console.log("bank detail chehan ", bankDetailsControllerScope.bankDetails);
            $scope.vm.spinner = false;
         	}
         	else{
         		requestParam = null;
         		$scope.vm.spinner = false;
         	}
          console.log("111111111111111", bankDetailsControllerScope.bankDetails);
        });
	}

		function decline(data) {
	    	var jsonFormatedData = JSON.parse(recorddetails);
	        console.log("jsonFormatedData", jsonFormatedData);
	          if(jsonFormatedData.response.success == 'true'){
	            var bankDetails = JSON.parse(jsonFormatedData.response.results);
	            bankDetailsControllerScope.bankDetails = bankDetails[0];
	           	console.log("recordDetails", bankDetails);
	        	}
  		}

		function uploadFile(){
	    	console.log('file is  ' );
	        var file = bankDetailsControllerScope.myFile;
	      
	        console.log(file);
	        var uploadUrl = "/fileUpload";
	        bankDetailsService.uploadFile(file, uploadUrl);
    	};

	}

	myApp.directive("compareTo", function() {
	    return {
	        require: "ngModel",
	        link: function(scope, element, attrs, ctrl) {

	            ctrl.$validators.compareTo = function(val) {
	                return val == scope.$eval(attrs.compareTo);
	            };

	            scope.$watch(attrs.compareTo, function() {
	                ctrl.$validate();
	            });
	        }
	    };
	});
