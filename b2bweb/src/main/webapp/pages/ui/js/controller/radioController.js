angular
  .module('app')
  .controller('radioController', radioController);

  radioController.$inject = ['$scope', 'radioService', '$rootScope', '$location'];

  function radioController($scope, radioService, $rootScope, $location) {
    var radioControllerScope = this;
        radioControllerScope.loader = true;
        radioControllerScope.getRadio = getRadio;
        radioControllerScope.knowMore = knowMore;
        radioControllerScope.getFilterLocation = getFilterLocation;
        radioControllerScope.getFilterLang = getFilterLang;
        radioControllerScope.sortBy = sortBy;
        radioControllerScope.addNewRadio = addNewRadio;
        radioControllerScope.addRadio = addRadio;     
        radioControllerScope.getFilterStation = getFilterStation;
        radioControllerScope.station = [];
        radioControllerScope.place = [];
        radioControllerScope.lang = [];

        var radioList = [];
        var dataLength = 0;
        
        var sortBy = 'topserch';

        getRadio();
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.stations = ['Aamar FM', 'Big FM', 'Club FM', 'Hit FM'];
        
        function knowMore(record){
            $location.url('/radioKnowmore');
            recordDetails = record;
        }

        function sortBy(arg){  
            dataLength = 0;  
            radioList = []; 
            if (arg == '') {
                sortBy = 'topserch';
            } else {
               sortBy = arg;
            }
            getRadio();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            radioList = []; 
            if (radioControllerScope.place.indexOf(arg) === -1) {
                radioControllerScope.place.push(arg);
            } else {
                radioControllerScope.place.splice(radioControllerScope.place.indexOf(arg), 1);
            }
            getRadio();       
        }

        

        function getFilterLang(arg){
            dataLength = 0;  
            radioList = [];   
            if (radioControllerScope.lang.indexOf(arg) === -1) {
                radioControllerScope.lang.push(arg);
            } else {
                radioControllerScope.lang.splice(radioControllerScope.lang.indexOf(arg), 1);
            } 
            getRadio();       
        }

        function getFilterStation(arg){
            dataLength = 0;  
            radioList = [];
            if (radioControllerScope.station.indexOf(arg) === -1) {
                radioControllerScope.station.push(arg);
            } else {
                radioControllerScope.station.splice(radioControllerScope.station.indexOf(arg), 1);
            } 
            getRadio();       
        }


            //get radio details
            function getRadio(){
                radioControllerScope.loader = true;
                console.log("hi");
              //  $rootScope.spinner = true;
               var filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    languages: (radioControllerScope.lang.length > 0 && radioControllerScope.lang.length != undefined ? radioControllerScope.lang : []),
                    geographies: (radioControllerScope.place.length > 0 && radioControllerScope.place.length != undefined ? radioControllerScope.place : []),
                    station: (radioControllerScope.station.length > 0 && radioControllerScope.station.length != undefined ? radioControllerScope.station : [])
                };
                radioService.getRadioDetail(filterPar).then(function(data){
                    var jsonFormatedData = JSON.parse(data);
                    if(jsonFormatedData.response.success == 'true'){
                        radioControllerScope.radioDetails = jsonFormatedData.response.results;
                        for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                            radioList.push(JSON.parse(radioControllerScope.radioDetails)[i]); 
                        }   
                        $scope.count = jsonFormatedData.response.count;      
                        dataLength += JSON.parse(jsonFormatedData.response.results).length;
                        radioControllerScope.radioDetails = radioList;
                        radioControllerScope.count = dataLength;
                        radioControllerScope.loader = false;
                    }
                },function(error){
                        console.log("error",error)
                })
            }
                

            radioControllerScope.mediaOptions = ['otherOptions','rjOptions','regularOptions'];
            radioControllerScope.otherOptions = ["timeCheck", "studioShift", "roadblock-NonPrimeTime", "roadblock-PrimeTime", "roadblock-AllDay", "sponsorshipTag", "contest", "halfPageHorizontal"];
            radioControllerScope.rjOptions = ["1"];
            radioControllerScope.regularOptions = ["primeTime", "nonPrimeTime", "mixedTime"];
            function addNewRadio(){
                $location.url('/radioNew');
            }

             var id= '';
             function addRadio(){
                radioControllerScope.radio.categoryId = [];
                radioControllerScope.radio.geography = [];  
                radioControllerScope.radio.keywords = [];           
                radioControllerScope.radio.uniqueId = '';  
                radioControllerScope.radio.thumbnail = ''; 
                radioControllerScope.radio.views = '1'; 
                radioControllerScope.radio._id = id; 
                console.log("chethannnn   ", radioControllerScope.radio);
                radioService.addRadio(radioControllerScope.radio).then(function(data){
                     var jsonFormatedData = JSON.parse(data);  
                     console.log('jsonFormatedData ',jsonFormatedData);                      
                    if(jsonFormatedData.response.success == 'true'){                           
                       var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                       console.log('respDats', respDate);
                       id = respDate._id['$oid'];
                    }
                },function(error){
                    console.log("error",error)
                });  
            }




            $scope.currentTab = 'radioGridView';

                // $scope.tabs = [{
                //         title: 'Grid View',
                //         url: 'radioGridView'
                // }, {
                //         title: 'List View',
                //         url: 'radioListView'
                // }];

                // $scope.currentTab = 'radioGridView';

                // $scope.onClickTab = function (tab) {
                //     $scope.currentTab = tab.url;
                //     dataLength = 0 ;
                //     radioList = [];
                //     radioControllerScope.radioDetails = [];
                //     getRadio();                     
                // }
                
                // $scope.isActiveTab = function(tabUrl) {
                //     return tabUrl == $scope.currentTab;
                // }
    
  }