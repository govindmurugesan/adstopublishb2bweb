angular
    .module('app')
    .controller('cinemaController', cinemaController);

    cinemaController.$inject = ['$scope', 'cinemaService', '$rootScope', '$location'];

    function cinemaController($scope, cinemaService, $rootScope, $location) {
        var cinemaControllerScope = this;
        cinemaControllerScope.loader = true;
        cinemaControllerScope.getCinemas = getCinemas;
        cinemaControllerScope.knowMore = knowMore;
        cinemaControllerScope.getFilterLocation = getFilterLocation;
        cinemaControllerScope.sortBy = sortBy;
        cinemaControllerScope.addNewCinema = addNewCinema;
        cinemaControllerScope.addCinema = addCinema;
        cinemaControllerScope.getFilterCinemachain = getFilterCinemachain;
        cinemaControllerScope.getFilterMallname = getFilterMallname;
        cinemaControllerScope.getFilterScreentype = getFilterScreentype;
        cinemaControllerScope.mallname = [];
        cinemaControllerScope.place = [];
        cinemaControllerScope.lang = [];
        cinemaControllerScope.cinemachain = [];
        cinemaControllerScope.screentype = [];


        var cinemaList = [];
        var dataLength = 0;
        
        var sortBy = 'topsearch';

        getCinemas();
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.cinemachains = ['A World', ' Aarthi Grand', 'Aasha', 'Abirami'];
        $scope.mallnames = ['3D Miniplex', ' A World', 'Aasha Cineplex', ' Abirami'];
        $scope.screentypes = ['Multiplex', 'Single Screen'];

        function knowMore(record){
            $location.url('/cinemaKnowmore');
            recordDetails = record;
        }

        
         function sortBy(arg){  
            console.log("sort by", arg);
            dataLength = 0;  
            cinemaList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getCinemas();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            cinemaList = []; 
            if (cinemaControllerScope.place.indexOf(arg) === -1) {
                cinemaControllerScope.place.push(arg);
            } else {
                cinemaControllerScope.place.splice(cinemaControllerScope.place.indexOf(arg), 1);
            }
            getCinemas();       
        }

        function getFilterCinemachain(arg){
            dataLength = 0;  
            cinemaList = [];   
            if (cinemaControllerScope.cinemachain.indexOf(arg) === -1) {
                cinemaControllerScope.cinemachain.push(arg);
            } else {
                cinemaControllerScope.cinemachain.splice(cinemaControllerScope.cinemachain.indexOf(arg), 1);
            } 
            getCinemas();       
        }

        function getFilterMallname(arg){
            dataLength = 0;  
            cinemaList = [];   
            if (cinemaControllerScope.mallname.indexOf(arg) === -1) {
                cinemaControllerScope.mallname.push(arg);
            } else {
                cinemaControllerScope.mallname.splice(cinemaControllerScope.mallname.indexOf(arg), 1);
            } 
            getCinemas();       
        }

        function getFilterScreentype(arg){
            dataLength = 0;  
            cinemaList = [];   
            if (cinemaControllerScope.screentype.indexOf(arg) === -1) {
                cinemaControllerScope.screentype.push(arg);
            } else {
                cinemaControllerScope.screentype.splice(cinemaControllerScope.screentype.indexOf(arg), 1);
            } 
            getCinemas();       
        }
        //get cinemas  details
        function getCinemas(){
            cinemaControllerScope.loader = true;
             console.log("22122");
           var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    cinemaChain: (cinemaControllerScope.cinemachain.length > 0 && cinemaControllerScope.cinemachain.length != undefined ? cinemaControllerScope.cinemachain : []),
                    geographies: (cinemaControllerScope.place.length > 0 && cinemaControllerScope.place.length != undefined ? cinemaControllerScope.place : []),
                    mallName: (cinemaControllerScope.mallname.length > 0 && cinemaControllerScope.mallname.length != undefined ? cinemaControllerScope.mallname : []),
                    screentype: (cinemaControllerScope.screentype.length > 0 && cinemaControllerScope.screentype.length != undefined ? cinemaControllerScope.screentype : [])
            };
            console.log("22", filterPar);
            cinemaService.getCinemaDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    cinemaControllerScope.cinemaDetail = jsonFormatedData.response.results;
                    console.log("11");
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                         cinemaList.push(JSON.parse(cinemaControllerScope.cinemaDetail)[i]);
                      }
                    $scope.count = jsonFormatedData.response.count;
                    console.log($scope.count);
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    cinemaControllerScope.cinemaDetail = cinemaList;
                    cinemaControllerScope.count = dataLength;
                    console.log('dataLength  ', dataLength)
                   cinemaControllerScope.loader = false;
                }

            },function(error){
                    console.log("error",error)
            })
        }
            
        $scope.options=['10SecMuteSlide','10SecAudioSlide','30SecVideo','60SecVideo'];
        function addNewCinema(){
              $location.url('/cinemaNew');
        }

        var id = '';
        function addCinema(record){  
        console.log('addcinemas id ', id);              
            record.urlSlug = '';
            record.uniqueId = '';  
            record._id = id; 
            cinemaService.addCinema(record).then(function(data){
                
                 var jsonFormatedData = JSON.parse(data);  
                 console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$id'];
                }
            },function(error){
                console.log("error",error)
            });  
            
        }

        $scope.currentTab = 'cinemaGridView';

        // $scope.tabs = [{
        //         title: 'Grid View',
        //         url: 'cinemaGridView'
        // }, {
        //         title: 'List View',
        //         url: 'cinemaListView'
        // }];

        // $scope.currentTab = 'cinemaGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     cinemaList = [];
        //     cinemaControllerScope.cinemaDetail = [];
        //     getCinemas();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }
    
    }

        

