angular
	.module('app')
	.controller('agencyDetailsController', agencyDetailsController);

	agencyDetailsController.$inject = ['$scope', 'agencyDetailsService', '$location', '$localStorage', '$rootScope'];

	function agencyDetailsController($scope, agencyDetailsService, $location, $localStorage, $rootScope) {
		var agencyDetailsControllerScope = this;

		agencyDetailsControllerScope.addvendordetail = addvendordetail;

		function addvendordetail(agencyDetails){
			console.log("GM Testing",agencyDetails);
			$rootScope.spinner=true;
			agencyDetailsService.addvendordetail(agencyDetails).then(function(data){
				var results = JSON.parse(data);		
				var success = results.response.success;
        		if(success == 'true'){
					$rootScope.spinner=false;
					console.log(data);
				}
		});

	}
}