angular
	.module('app')
	.controller('menusController', menusController);

	menusController.$inject = ['$scope', '$localStorage', '$location', '$cookies','$rootScope'];

	function menusController($scope, $localStorage, $location,  $cookies,$rootScope) {
		var menusControllerScope = this;
			
		menusControllerScope.logout = logout;
		menusControllerScope.login = login;

		/*menusControllerScope.usrName = $localStorage.jwtToken.sub;	*/
		$scope.vm.divShow = "login";
		getCookiesInfo();
		function getCookiesInfo(){
			var cookiesDetail = $cookies.getObject('userInfoCookies');
			console.log("cookiesDetail",$cookies.getObject('userInfoCookies'));
			if(cookiesDetail){
				console.log("menusControllerScope.usrName",cookiesDetail);
				menusControllerScope.userName = cookiesDetail.sub;
				console.log("usrName",menusControllerScope.userName);
				$location.url('/radio');
			}else{
				$location.url('/');
			}


		}
		
		

		$rootScope.$on("CallParentMethod", function(){
          getCookiesInfo();
        });

	
	
		function logout(){
			console.log("My logout Method called ...");
			/*$localStorage.jwtToken = '';
			$localStorage.token = '';
			menusControllerScope.usrName = '';*/
			$cookies.remove('userInfoCookies');
			menusControllerScope.userName = ""
			/*$location.url('/topmenus');*/

			$location.url('/');
		}

		function login(){
			$scope.vm.divShow = "";
			$scope.vm.loginUser = {};
		/*	$scope.vm.userRegistrationDetail = {};*/
			$scope.vm.loginErrorMsg = false;
			/*$scope.vm.loginMessage = false;*/
			$('#loginDescp').modal('show');
			$scope.vm.divShow = "login";
		}
	}
