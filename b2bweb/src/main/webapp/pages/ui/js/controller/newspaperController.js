angular
	.module('app')
	.controller('newspaperController', newspaperController);

	newspaperController.$inject = ['$scope', 'newspaperService', '$rootScope', '$location'];

	function newspaperController($scope, newspaperService, $rootScope, $location) {
		var newspaperControllerScope = this;
        newspaperControllerScope.loader = true;
        newspaperControllerScope.getNewspaper = getNewspaper;
        newspaperControllerScope.knowMore = knowMore;
        newspaperControllerScope.getFilterLocation = getFilterLocation;
        newspaperControllerScope.getFilterLang = getFilterLang;
        newspaperControllerScope.sortBy = sortBy;
        newspaperControllerScope.addNewNewspaper = addNewNewspaper;
        newspaperControllerScope.addNewspaper = addNewspaper;
        newspaperControllerScope.getFilterCategory = getFilterCategory;
        newspaperControllerScope.getFilterPublication = getFilterPublication;
        newspaperControllerScope.category = [];
        newspaperControllerScope.place = [];
        newspaperControllerScope.lang = [];
        newspaperControllerScope.publication = [];
        


        var newspaperList = [];
        var dataLength = 0;
        
        var sortBy = 'circulation';

        getNewspaper();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.categories = ['Environmental', 'Spiritual', 'Sports', 'General Interest'];
        $scope.publications = [' Andhra Jyothi ', 'Andhra Prabha ', 'Asian Age ', ' Asomiya Pratidin '];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];

        function knowMore(record){
            $location.url('/newspaperKnowmore');
            recordDetails= record;
        }

        
        function sortBy(arg){  
            dataLength = 0;  
            newspaperList = []; 
            if (arg == '') {
                sortBy = 'circulation';
            } else {
               sortBy = arg;
            }
            getNewspaper();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            newspaperList = []; 
            if (newspaperControllerScope.place.indexOf(arg) === -1) {
                newspaperControllerScope.place.push(arg);
            } else {
               newspaperControllerScope.place.splice(newspaperControllerScope.place.indexOf(arg), 1);
            }
            getNewspaper();       
        }

        
        function getFilterLang(arg){
            dataLength = 0;  
            newspaperList = [];   
            if (newspaperControllerScope.lang.indexOf(arg) === -1) {
                newspaperControllerScope.lang.push(arg);
            } else {
                newspaperControllerScope.lang.splice(newspaperControllerScope.lang.indexOf(arg), 1);
            } 
            getNewspaper();       
        }

        function getFilterCategory(arg){
            dataLength = 0;
            newspaperList =[];
            if(newspaperControllerScope.category.indexOf(arg) === -1) {
                newspaperControllerScope.category.push(arg);
            }else{
                newspaperControllerScope.category.splice(newspaperControllerScope.category.indexOf(arg), 1);
            }
            getNewspaper(); 
        }


        function getFilterPublication(arg){
            dataLength = 0;
            newspaperList =[];
            if(newspaperControllerScope.publication.indexOf(arg) === -1) {
                newspaperControllerScope.publication.push(arg);
            }else{
                newspaperControllerScope.publication.splice(newspaperControllerScope.publication.indexOf(arg), 1);
            }
            getNewspaper(); 
        }
        //get newspaper details
        function getNewspaper(){
            newspaperControllerScope.loader = true;
            var  filterPar = {
                offset: dataLength,
                sortBy: sortBy,
                languages: (newspaperControllerScope.lang.length > 0 && newspaperControllerScope.lang.length != undefined ? newspaperControllerScope.lang : []),
                geographies: (newspaperControllerScope.place.length > 0 && newspaperControllerScope.place.length != undefined ? newspaperControllerScope.place : []),
                categories: (newspaperControllerScope.category.length > 0 && newspaperControllerScope.category.length != undefined ? newspaperControllerScope.category : []),
                publications: (newspaperControllerScope.publication.length > 0 && newspaperControllerScope.publication.length != undefined ? newspaperControllerScope.publication : []),
                frequencies: ""
            };
            console.log(filterPar);
            newspaperService.getNewspaperDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    newspaperControllerScope.newspaperDetails = jsonFormatedData.response.results;
                    newspaperList.push(JSON.parse(newspaperControllerScope.newspaperDetails));    
                    $scope.count = jsonFormatedData.response.count;                    
                    //console.log(newspaperList);
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    newspaperControllerScope.newspaperDetails = newspaperList;
                    newspaperControllerScope.count = dataLength;
                    newspaperControllerScope.loader = false;
                }
            },function(error){
                    console.log("error",error)
            })
        }
            
        $scope.currentTab = 'newsPaperGridView';

        // $scope.tabs = [{
        //         title: 'Grid View',
        //         url: 'newsPaperGridView'
        // }, {
        //         title: 'List View',
        //         url: 'newsPaperListView'
        // }];

        // $scope.currentTab = 'newsPaperGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     newspaperList = [];
        //     newspaperControllerScope.newspaperDetails = [];
        //     getNewspaper();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }



        newspaperControllerScope.mediaOptions = ['regularOptions', 'otherOptions' ];
        newspaperControllerScope.regularOptions = ["anyPage", "lastPage", "page3", "page1"];
        newspaperControllerScope.otherOptions = ["skyBus", "pointerAd", "jacketFrontInside", "jacketFrontPage"];

        

        console.log("out  ", newspaperControllerScope.mediaOptions);
        function addNewNewspaper(){ 
             console.log("in  ", newspaperControllerScope.options);               
            $location.url('/newspaperNew');
        }

        var id= '';
        function addNewspaper(){ 
            newspaperControllerScope.newspaper.categoryId = [];
            newspaperControllerScope.newspaper.geography = [];
            newspaperControllerScope.newspaper.urlSlug = '';
            newspaperControllerScope.newspaper.uniqueId = '';  
            newspaperControllerScope.newspaper._id = id; 
            console.log("chethannnn   ", newspaperControllerScope.newspaper);
            newspaperService.addNewspaper(newspaperControllerScope.newspaper).then(function(data){
                
                 var jsonFormatedData = JSON.parse(data);  
                 console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$id'];
                }
            },function(error){
                console.log("error",error)
            });  
        }

	


	}

		

