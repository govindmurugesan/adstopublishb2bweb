 
angular
       .module('app')
       .config(config);
    	config.$inject = ['$routeProvider']

    	function config($routeProvider){
	    	   $routeProvider.
	           
	           when('/', {
	              templateUrl: 'pages/home.html',
	              controller: 'homeController as homeControllerScope'
	           }).
	           when('/dashboard', {
	              templateUrl: 'pages/dashboard.html',
	           }).
	           when('/password', {
	              templateUrl: 'pages/password.html',
	           }).
	           when('/home', {
	              templateUrl: 'pages/home.html',
	           }).
	           when('/signupSuccess', {
	              templateUrl: 'pages/signupSuccess.html',
	           }).
	           when('/resetPassword', {
	              templateUrl: 'pages/resetPassword.html',
	           }).
	           when('/vendorfaqs', {
	              templateUrl: 'pages/faqs.html',
	           }).
	           when('/bankDetails', {
	              templateUrl: 'pages/bankDetails.html',
	             controller: 'bankDetailsController as bankDetailsControllerScope',
	             params: {
	              	recordDetails: null
	              }
	           }).
	           when('/businessDetails', {
	              templateUrl: 'pages/businessDetails.html',
	             controller: 'businessDetailsController as businessDetailsControllerScope',
	             params: {
	              	recordDetails: null
	              }
	           }).
	            when('/agencyDetails', {
	              templateUrl: 'pages/storeDetails.html',
	             controller: 'agencyDetailsController as agencyDetailsControllerScope'
	           }).
	            when('/radio', {
	              templateUrl: 'pages/radio.html',
	              controller: 'radioController as radioControllerScope'
	           }).
	            when('/radioNew', {
	              templateUrl: 'pages/radioNew.html',
	              controller: 'radioController as radioControllerScope'
	           }).
	            when('/radioKnowmore',{
                    templateUrl: 'pages/radioKnowmore.html',
                    controller: 'radioKnowmoreController as radioKnowmoreControllerScope',
                    params: {
		              recordDetails: null
		            }
                }).
	            when('/newspaper', {
	              templateUrl: 'pages/newspaper.html',
	              controller: 'newspaperController as newspaperControllerScope'
	           }).	            
	            when('/newspaperNew',{
                    templateUrl: 'pages/newspaperNew.html',
                    controller: 'newspaperController as newspaperControllerScope'
                }).
                when('/newspaperKnowmore',{
                    templateUrl: 'pages/newspaperKnowmore.html',
                    controller: 'newspaperKnowmoreController as newspaperKnowmoreControllerScope',
                    params: {
		              recordDetails: null
		            }
                }).
	            when('/leftMenus', {
	              templateUrl: 'pages/leftMenus.html',
	              controller: 'leftMenusController as leftMenusControllerScope'
	           }).
	            when('/knowMore', {
	              templateUrl: 'pages/knowmore.html',
	              controller: 'knowMoreController as knowMoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
	             when('/myProfile',{
                    templateUrl: 'pages/myProfile.html',
                    controller: 'myProfileController as myProfileControllerScope'
                }).
	            when('/error', {
	              templateUrl: 'pages/error.html',
	              // controller: 'newspaperController as newspaperControllerScope1'
	           }).
	            when('/magazine',{
	            	templateUrl: 'pages/magazine.html',
	            	controller: 'magazineController as magazineControllerScope'
	            }).
	            when('/magazineNew',{
	            	templateUrl: 'pages/magazineNew.html',
	            	controller: 'magazineController as magazineControllerScope'
	            }).
	             when('/magazineKnowMore', {
	              templateUrl: 'pages/magazineKnowMore.html',
	              controller: 'magazineKnowMoreController as magazineKnowMoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
	            when('/profile',{
	            	templateUrl: 'pages/profile.html',
	            	controller: 'profileController as profileControllerScope',
	            	params: {
	              		recordDetails: null
	              	}
	            }).
 				when('/airlineAirport',{
	            	templateUrl: 'pages/airlineAndAirport.html',
	            	controller: 'airlineAndAirportController as airlineAndAirportControllerScope'
	            }).
	            when('/airlineAndAirportNew',{
	            	templateUrl: 'pages/airlineAndAirportNew.html',
	            	controller: 'airlineAndAirportController as airlineAndAirportControllerScope'
	            }).
 				when('/cinema',{
	            	templateUrl: 'pages/cinema.html',
	            	controller: 'cinemaController as cinemaControllerScope'
	            }).
	            when('/cinemaNew',{
	            	templateUrl: 'pages/cinemaNew.html',
	            	controller: 'cinemaController as cinemaControllerScope'
	            }).
	            when('/cinemaKnowmore',{
	            	templateUrl: 'pages/cinemaKnowmore.html',
	            	controller: 'cinemaKnowmoreController as cinemaKnowmoreControllerScope',
	            	params: {
	              		recordDetails: null
	                }
	            }).
 				when('/nontraditional',{
	            	templateUrl: 'pages/nonTraditional.html',
	            	controller: 'nonTraditionalController as nonTraditionalControllerScope'
	            }).
	            when('/nonTraditionalNew',{
	            	templateUrl: 'pages/nonTraditionalNew.html',
	            	controller: 'nonTraditionalController as nonTraditionalControllerScope'
	            }).
	            when('/nontraditionalKnowmore',{
	            	templateUrl: 'pages/nontraditionalKnowmore.html',
	            	controller: 'nonTraditionalknowmoreController as nonTraditionalknowmoreControllerScope',
	            	params: {
	              		recordDetails: null
	                }
	            }).
	            when('/outdoor',{
	            	templateUrl: 'pages/outdoor.html',
	            	controller: 'outdoorController as outdoorControllerScope'
	            }).
	            when('/outdoorNew',{
                    templateUrl: 'pages/outdoorNew.html',
                    controller: 'outdoorController as outdoorControllerScope'
                }).
                when('/outdoorKnowmore', {
	              templateUrl: 'pages/outdoorKnowmore.html',
	              controller: 'outdoorKnowmoreController as outdoorKnowmoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	            }).
	            when('/digital',{
	            	templateUrl: 'pages/digital.html',
	            	controller: 'digitalController as digitalControllerScope'
	            }).
	            when('/digitalNew',{
	            	templateUrl: 'pages/digitalNew.html',
	            	controller: 'digitalController as digitalControllerScope'
	            }).
	            when('/digitalKnowmore',{
                    templateUrl: 'pages/digitalKnowmore.html',
                    controller: 'digitalKnowmoreController as digitalKnowmoreControllerScope',
                    params: {
		              	recordDetails: null
		            }
                }).

	            when('/television',{
                    templateUrl: 'pages/television.html',
                    controller: 'televisionController as televisionControllerScope'
                }).
                when('/order',{
                    templateUrl: 'pages/order.html',
                    controller: 'orderController as orderControllerScope'
                }).
                when('/myQuotes',{
                    templateUrl: 'pages/myQuotes.html',
                    controller: 'myQuotesController as myQuotesControllerScope'
                }).
                when('/televisionNew',{
                    templateUrl: 'pages/televisionNew.html',
                    controller: 'televisionController as televisionControllerScope'
                }).
                 when('/televisionKnowmore',{
                    templateUrl: 'pages/televisionKnowmore.html',
                    controller: 'televisionKnowmoreController as televisionKnowmoreControllerScope',
                    params: {
		              	recordDetails: null
		            }
                }).
                when('/airlineAirportKnowmore', {
	              templateUrl: 'pages/airlineAirportKnowmore.html',
	              controller: 'airlineAirportKnowmoreController as airlineAirportKnowmoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
               
                
	           otherwise({
	              redirectTo: '/'
	           });

	          
        }
    
