 
angular
       .module('app')
       .config(config);
    	config.$inject = ['$routeProvider']

    	function config($routeProvider){
	    	   $routeProvider.
	           
	           when('/', {
	              templateUrl: 'templates/home.html',
	              controller: 'homeController as homeControllerScope'
	           }).
	           when('/dashboard', {
	              templateUrl: 'templates/dashboard.html',
	           }).
	           when('/home', {
	              templateUrl: 'templates/home.html',
	           }).
	           when('/signupSuccess', {
	              templateUrl: 'templates/signupSuccess.html',
	           }).
	           when('/resetPassword', {
	              templateUrl: 'templates/resetPassword.html',
	           }).
	           when('/vendorfaqs', {
	              templateUrl: 'templates/faqs.html',
	           }).
	           
	            when('/radio', {
	              templateUrl: 'templates/radio.html',
	              controller: 'radioController as radioControllerScope'
	           }).
	            
	            when('/radioKnowmore',{
                    templateUrl: 'templates/radioKnowmore.html',
                    controller: 'radioKnowmoreController as radioKnowmoreControllerScope',
                    params: {
		              recordDetails: null
		            }
                }).
	            when('/newspaper', {
	              templateUrl: 'templates/newspaper.html',
	              controller: 'newspaperController as newspaperControllerScope'
	           }).	            
	            
                when('/newspaperKnowmore',{
                    templateUrl: 'templates/newspaperKnowmore.html',
                    controller: 'newspaperKnowmoreController as newspaperKnowmoreControllerScope',
                    params: {
		              recordDetails: null
		            }
                }).
	           

	            when('/knowMore', {
	              templateUrl: 'templates/knowmore.html',
	              controller: 'knowMoreController as knowMoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
	            
	            when('/error', {
	              templateUrl: 'templates/error.html',
	              // controller: 'newspaperController as newspaperControllerScope1'
	           }).
	            when('/magazine',{
	            	templateUrl: 'templates/magazine.html',
	            	controller: 'magazineController as magazineControllerScope'
	            }).
	            

	             when('/magazineKnowMore', {
	              templateUrl: 'templates/magazineKnowMore.html',
	              controller: 'magazineKnowMoreController as magazineKnowMoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
	           
 				when('/airlineAirport',{
	            	templateUrl: 'templates/airlineAndAirport.html',
	            	controller: 'airlineAndAirportController as airlineAndAirportControllerScope'
	            }).
	            
 				when('/cinema',{
	            	templateUrl: 'templates/cinema.html',
	            	controller: 'cinemaController as cinemaControllerScope'
	            }).
	            when('/order',{
	            	templateUrl: 'templates/order.html',
	            	controller: 'orderController as orderControllerScope'
	            }).
	            when('/myQuotes',{
	            	templateUrl: 'templates/myQuotes.html',
	            	controller: 'myQuotesController as myQuotesControllerScope'
	            }).
	           
	            when('/cinemaKnowmore',{
	            	templateUrl: 'templates/cinemaKnowmore.html',
	            	controller: 'cinemaKnowmoreController as cinemaKnowmoreControllerScope',
	            	params: {
	              		recordDetails: null
	                }
	            }).
 				when('/nontraditional',{
	            	templateUrl: 'templates/nonTraditional.html',
	            	controller: 'nonTraditionalController as nonTraditionalControllerScope'
	            }).
	           
	            when('/nontraditionalKnowmore',{
	            	templateUrl: 'templates/nontraditionalKnowmore.html',
	            	controller: 'nonTraditionalknowmoreController as nonTraditionalknowmoreControllerScope',
	            	params: {
	              		recordDetails: null
	                }
	            }).
	            when('/outdoor',{
	            	templateUrl: 'templates/outdoor.html',
	            	controller: 'outdoorController as outdoorControllerScope'
	            }).
	           
                when('/outdoorKnowmore', {
	              templateUrl: 'templates/outdoorKnowmore.html',
	              controller: 'outdoorKnowmoreController as outdoorKnowmoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	            }).
	            when('/digital',{
	            	templateUrl: 'templates/digital.html',
	            	controller: 'digitalController as digitalControllerScope'
	            }).
	            
	            when('/digitalKnowmore',{
                    templateUrl: 'templates/digitalKnowmore.html',
                    controller: 'digitalKnowmoreController as digitalKnowmoreControllerScope',
                    params: {
		              	recordDetails: null
		            }
                }).

	            when('/television',{
                    templateUrl: 'templates/television.html',
                    controller: 'televisionController as televisionControllerScope'
                }).
                
                 when('/televisionKnowmore',{
                    templateUrl: 'templates/televisionKnowmore.html',
                    controller: 'televisionKnowmoreController as televisionKnowmoreControllerScope',
                    params: {
		              	recordDetails: null
		            }
                }).
                when('/airlineAirportKnowmore', {
	              templateUrl: 'templates/airlineAirportKnowmore.html',
	              controller: 'airlineAirportKnowmoreController as airlineAirportKnowmoreControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
                when('/bookingSummary', {
	              templateUrl: 'templates/bookingSummary.html',
	              controller: 'bookingSummaryController as bookingSummaryControllerScope',
	              params: {
	              	recordDetails: null
	              }
	           }).
                
	           otherwise({
	              redirectTo: '/'
	           });

	          
        }
    
