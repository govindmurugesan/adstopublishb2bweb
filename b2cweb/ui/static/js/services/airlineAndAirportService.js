angular.module('app').factory('airlineAndAirportService', airlineAndAirportService);

	airlineAndAirportService.$inject =  ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

	function airlineAndAirportService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
		var service = {
		        getAirport: getAirport,		        
		        addAirline: addAirline
		    };

		 return service;


		function addAirline(requestParam){	
			 console.log('addAirline service  '+requestParam);
			 console.log('addAirline service111  '+JSON.stringify(requestParam));
	        var deferred = $q.defer();
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.AIRLINE_AND_AIRPORT + commonService.ADD_AIRLINE_AND_AIRPORT + '/' + btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity,
	            headers: {  'Content-Type': undefined}
	        }).success(function (data, status, headers, config) {
	            console.log("response in ADD_AIRLINE_AND_AIRPORT");          
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            deferred.reject(data);
	        }); 
	        return deferred.promise;
		}

		function getAirport(parameters){
			console.log(parameters.languages);

			var requestParam = {
       			offset: parameters.offset == undefined ? '':parameters.offset,
       			limit:'',
		        filters:{
		            categories:[],
		            geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
		            languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
		            frequencies:[],
		            targetGroups:[],
		            mediaOptions:[]
	            },
	            sortBy: parameters.sortBy,
	            recommended:''
	        }

			console.log('parameters.offset', parameters);
			var deferred = $q.defer();
			console.log(constantsService.url);
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.AIRLINE_AND_AIRPORT + commonService.GET_AIRLINE_AND_AIRPORT + '/' + btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity,               
	            headers: { 'Content-Type': undefined }
	            
	        }).success(function (data, status, headers, config) {
	            console.log("magazine DEtails  ");
	            //$localStorage.token = response.data;
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            console.error('magazine Error while fetching Users    ', data);
	            //$localStorage.token = '';
	            deferred.reject(data);
	        });       
            return deferred.promise;
        }

	}