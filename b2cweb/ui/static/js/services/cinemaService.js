angular
.module('app')
.factory('cinemaService', cinemaService);

cinemaService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function cinemaService(constantsService, $q, $http, commonService) {
	var service = {
        getCinemaDetail: getCinemaDetail,
        addCinema: addCinema
    };
    return service;

    function getCinemaDetail(parameters){
    	console.log(parameters.offset)
        var requestParam = {
			offset: parameters.offset == undefined ? '':parameters.offset,
                limit:'',
                filters:{
                    categories:[],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    cinemaChain: (parameters.cinemaChain.length > 0 && parameters.cinemaChain != undefined) ? parameters.cinemaChain : [],
                    mallName:(parameters.mallName.length > 0 && parameters.mallName != undefined) ? parameters.mallName : [],
                    screentype:(parameters.screentype.length > 0 && parameters.screentype != undefined) ? parameters.screentype : []
                    
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url + commonService.CINEMA + commonService.GET_CINEMA + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log(data);
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }

    function addCinema(requestParam){
        console.log('addCinema service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CINEMA + commonService.ADD_CINEMA + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {  
            console.log("response in addCinema");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }
}