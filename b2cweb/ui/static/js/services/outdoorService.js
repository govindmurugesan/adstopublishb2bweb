angular
.module('app')
.factory('outdoorService', outdoorService);

outdoorService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function outdoorService(constantsService, $q, $http, commonService) {
	var service = {
        getOutdoorDetail:getOutdoorDetail,
        addOutdoor:addOutdoor
    };
    return service;

    function getOutdoorDetail(parameters){
    	console.log(parameters.offset)
        var requestParam = {
			offset: parameters.offset == undefined ? '':parameters.offset,
                limit:'',
                filters:{
                    categories:[],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    mediaType:(parameters.mediaType.length > 0 && parameters.mediaType != undefined) ? parameters.mediaType : [],
                    size:(parameters.sizee != undefined && parameters.sizee.length > 0  ) ? parameters.sizee : []
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url + commonService.OUTDOOR + commonService.GET_OUTDOOR + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            //console.log(data);
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }


    function addOutdoor(requestParam){
        console.log(' service add');
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.OUTDOOR + commonService.ADD_OUTDOOR + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {  
            console.log("response in server");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }


}