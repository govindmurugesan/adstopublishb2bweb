angular
.module('app')
.factory('orderService', orderService);

orderService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function orderService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        addOrder: addOrder,
        getMyOrderQuotes: getMyOrderQuotes,
        getNewOrders: getNewOrders,
        getProccedOrders: getProccedOrders,
        orderProcced: orderProcced

    };
    return service;

    function addOrder(requestParam){
        console.log('add order service sending  '+requestParam);

        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.ADD_CUSTOMERQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

   function getMyOrderQuotes(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.GET_CUSTOMER_ORDERDQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in quotes",data);          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }

    function getNewOrders(requestParam){
        console.log('add order service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.GET_NEWCUSTOMERQUOTES + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {        
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }


    function orderProcced(orderSummery){
       console.log('add order service  '+orderSummery);
       var deferred = $q.defer();
       $http({
           method : 'GET',
           url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.ADDORDERSUMMARY + '/' + btoa(JSON.stringify(orderSummery)),
           transformRequest: angular.identity,
           transformResponse: angular.identity,
           headers: {  'Content-Type': undefined}
       }).success(function (data, status, headers, config) {        
           deferred.resolve(data);
       }).error(function (data, status) {
           deferred.reject(data);
       });
       return deferred.promise;
   }

    function getProccedOrders(requestParam){
       console.log('add order service  '+requestParam);
       var deferred = $q.defer();
       $http({
           method : 'GET',
           url :  constantsService.url + commonService.CUSTOMERQUOTES + commonService.GET_PROCCEDORDERS + '/' + btoa(JSON.stringify(requestParam)),
           transformRequest: angular.identity,
           transformResponse: angular.identity,
           headers: {  'Content-Type': undefined}
       }).success(function (data, status, headers, config) {
           deferred.resolve(data);
       }).error(function (data, status) {
           deferred.reject(data);
       });
       return deferred.promise;
    }
       

    
}