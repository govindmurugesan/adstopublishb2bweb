angular.module('app').factory('magazineService', magazineService);

	magazineService.$inject =  ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

	function magazineService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
		var service = {
		        getMagazine: getMagazine,
		        addMagazine: addMagazine
		    };

		 return service;
		 
		 function addMagazine(requestParam){
	        console.log('addMagazine service  '+JSON.stringify(requestParam));
	        var deferred = $q.defer();
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.MAGAZINE + commonService.ADD_MAGAZINE + '/' + btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity,
	            headers: {  'Content-Type': undefined}
	        }).success(function (data, status, headers, config) {
	            console.log("response in ADD_NEWSPAPER");          
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            deferred.reject(data);
	        }); 
	        return deferred.promise;

	    }

		
		function getMagazine(parameters){
			console.log(parameters.languages);

			var requestParam = {
       			offset: parameters.offset == undefined ? '':parameters.offset,
       			limit:'',
		        filters:{
		            categories:[],
		            geographies:[],
		            languages:parameters.languages,
		            frequencies:[],
		            targetGroups:[],
		            mediaOptions:[]
	            },
	            sortBy:'views',
	            recommended:''
	        }
			console.log('parameters.offset', parameters.offset);
			var deferred = $q.defer();
			console.log(constantsService.url);
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.MAGAZINE + commonService.GET_MAGAZINE+ '/' + btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity, 
	            transformResponse: angular.identity,               
	            headers: { 'Content-Type': undefined }
	            
	        }).success(function (data, status, headers, config) {
	            console.log("magazine DEtails  ");
	            //$localStorage.token = response.data;
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            console.error('magazine Error while fetching Users    ', data);
	            //$localStorage.token = '';
	            deferred.reject(data);
	        });       
            return deferred.promise;
        }

        

	}