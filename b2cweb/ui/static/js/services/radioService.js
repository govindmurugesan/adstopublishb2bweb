angular
.module('app')
.factory('radioService', radioService);

radioService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function radioService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        getRadioDetail: getRadioDetail,
        addRadio:addRadio
    };
    return service;

    function getRadioDetail(parameters){
    	
        var requestParam = {
            offset: parameters.offset == undefined ? '':parameters.offset,
                   limit:'',
                filters:{
                    station:[],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[]
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
    	
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url+ commonService.RADIO_SERVICE + commonService.RADIO_SERVICE_GET + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }

    function addRadio(requestParam){
        console.log('addRadio service  '+JSON.stringify(requestParam));
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.RADIO_SERVICE + commonService.ADD_RADIO + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }
}