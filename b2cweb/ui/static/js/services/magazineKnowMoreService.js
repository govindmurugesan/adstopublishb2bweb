angular.module('app').factory('magazineKnowMoreService', magazineKnowMoreService);

	magazineKnowMoreService.$inject =  ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

	function magazineKnowMoreService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
		var service = {
		    updateMagazine: updateMagazine
		}
		return service;

		function updateMagazine(requestParam){
			var deferred = $q.defer();			
			console.log("22222222 ", requestParam);
	        $http({
	            method : 'GET',
	            url :  constantsService.url + commonService.MAGAZINE + commonService.ADD_MAGAZINE +'/'+ btoa(JSON.stringify(requestParam)),
	            transformRequest: angular.identity,
	            transformResponse: angular.identity,               
	            headers: { 'Content-Type': undefined }
	        }).success(function (data, status, headers, config) {
	            console.log("AA update  ");
	            deferred.resolve(data);
	        }).error(function (data, status) {
	            console.error('AA Error while fetching Users    ', data);
	            deferred.reject(data);
	        });
            return deferred.promise;
   		}

		
	}