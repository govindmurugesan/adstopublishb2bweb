angular
.module('app')
.factory('televisionService', televisionService);

televisionService.$inject = ['$http', '$q', 'constantsService','commonService', '$localStorage', '$rootScope'];

function televisionService($http, $q, constantsService, commonService, $localStorage, $rootScope) {
    var service = {
        getTelevisionDetail: getTelevisionDetail,
        addTelevision: addTelevision
    };
    return service;

    function getTelevisionDetail(parameters){
        var requestParam = {
            offset: parameters.offset == undefined ? '':parameters.offset,
                limit:'',
                filters:{
                    categories:[],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    channelgenre:(parameters.channelgenre.length > 0 && parameters.channelgenre != undefined)  ? parameters.channelgenre : [],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[]
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
    	
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url+ commonService.TELEVISION_SERVICE + commonService.TELEVISION_SERVICE_GET + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;

    }

    function addTelevision(requestParam){
        console.log('addMagazine service   '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.TELEVISION_SERVICE + commonService.ADD_TELEVISION + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;

    }
}