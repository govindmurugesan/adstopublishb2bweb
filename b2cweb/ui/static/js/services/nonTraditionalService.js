angular
.module('app')
.factory('nonTraditionalService', nonTraditionalService);

nonTraditionalService.$inject = ['constantsService', '$q', '$http', 'commonService'];

function nonTraditionalService(constantsService, $q, $http, commonService) {
	var service = {
        getNonTraditionalDetail: getNonTraditionalDetail,
        addnonTraditional: addnonTraditional
    };
    return service;

    function getNonTraditionalDetail(parameters){
    	console.log(parameters.offset)
        var requestParam = {
			offset: parameters.offset == undefined ? '':parameters.offset,
                limit:'',
                filters:{
                    categories:[],
                    geographies: (parameters.geographies.length > 0 && parameters.geographies != undefined) ? parameters.geographies : [],
                    languages: (parameters.languages.length > 0 && parameters.languages != undefined)  ? parameters.languages : [],
                    frequencies:[],
                    targetGroups:[],
                    mediaOptions:[]
                },
                sortBy: parameters.sortBy,
                recommended:''
        };
        var deferred = $q.defer();
    	
       $http({
            method : 'GET',
            url :  constantsService.url + commonService.NON_TRADITIONAL + commonService.GET_NON_TRADITIONAL + '/' + btoa(JSON.stringify(requestParam)),
			transformRequest: angular.identity, 
			transformResponse: angular.identity,
			headers: { 	'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log(data);
			deferred.resolve(data);
		}).error(function (data, status) {
			deferred.reject(data);
		});        
        return deferred.promise;
    }

     function addnonTraditional(requestParam){
        console.log('addRadio service  '+requestParam);
        var deferred = $q.defer();
        $http({
            method : 'GET',
            url :  constantsService.url + commonService.NON_TRADITIONAL + commonService.ADD_NON_TRADITIONAL + '/' + btoa(JSON.stringify(requestParam)),
            transformRequest: angular.identity, 
            transformResponse: angular.identity,
            headers: {  'Content-Type': undefined}
        }).success(function (data, status, headers, config) {
            console.log("response in ADD_NEWSPAPER");          
            deferred.resolve(data);
        }).error(function (data, status) {
            deferred.reject(data);
        }); 
        return deferred.promise;
    }
}