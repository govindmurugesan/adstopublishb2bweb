angular
  .module('app')
  .controller('televisionController', televisionController);

  televisionController.$inject = ['$scope', 'televisionService', '$rootScope', '$location'];

  function televisionController($scope, televisionService, $rootScope, $location) {
    var televisionControllerScope = this;
        televisionControllerScope.loader = true;
        televisionControllerScope.getTelevision = getTelevision;
        televisionControllerScope.knowMore = knowMore;
        televisionControllerScope.getFilterLocation = getFilterLocation;
        televisionControllerScope.getFilterLang = getFilterLang;
        televisionControllerScope.sortBy = sortBy;        

        var televisionList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = 'topsearch';

        getTelevision();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.channelgenres = [' Comedy ', ' Cookery ', 'Entertainment ', 'Fashion And Lifestyle '];
        

        televisionControllerScope.mediaOptions = ['regularOptions','planningOptions'];
        televisionControllerScope.regularOptions = ['mediaOption1'];
        televisionControllerScope.planningOptions = ['allDayPlan']
        
        function knowMore(record){
            $location.url('/televisionKnowmore');
            recordDetails= record;
        }
        
        function sortBy(arg){  
            dataLength = 0;  
            televisionList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getTelevision();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            televisionList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getTelevision();       
        }

       
        function getFilterLang(arg){
            dataLength = 0;  
            televisionList = [];   
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getTelevision();       
        }

        //get television details
        function getTelevision(){
        televisionControllerScope.loader = true;
           var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                    geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : []),
                    channelgenre:"",
                    
            };

            televisionService.getTelevisionDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    televisionControllerScope.televisionDetails = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                    televisionList.push(JSON.parse(televisionControllerScope.televisionDetails)[i]); 
                        }                        
                    $scope.count = jsonFormatedData.response.count;
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    televisionControllerScope.televisionDetails = televisionList;
                    televisionControllerScope.count = dataLength;
                    televisionControllerScope.loader = false;
                }

            },function(error){
                    console.log("error",error)
            })
        }
        $scope.currentTab = 'televisionGridView';

        // $scope.tabs = [{
        //     title: 'Grid View',
        //     url: 'televisionGridView'
        // }, {
        //     title: 'List View',
        //     url: 'televisionListView'
        // }];

        // $scope.currentTab = 'televisionGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     televisionList = [];
        //     televisionControllerScope.televisionDetails = [];
        //     getTelevision();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }

    
}