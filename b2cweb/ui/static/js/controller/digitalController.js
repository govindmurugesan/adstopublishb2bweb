angular.module('app').controller('digitalController', digitalController);

	digitalController.$inject = ['$scope', 'digitalService', '$location', '$localStorage',  '$rootScope'];

	function digitalController($scope, digitalService, $location, $localStorage, $rootScope) {
		
		var digitalControllerScope = this;
    digitalControllerScope.loader = true;
		digitalControllerScope.getDigital = getDigital;
		digitalControllerScope.dropboxitemselected = dropboxitemselected;
		digitalControllerScope.knowMore = knowMore;
		digitalControllerScope.addNewDigital = addNewDigital;
		digitalControllerScope.addDigital = addDigital;
		digitalControllerScope.addMore = addMore;

		var dataLength = 0 ;		
		var digitalList =[];	
		$scope.place = [];
	    $scope.lang = [];	

		
        getDigital();

        $scope.mediums = ['App ', ' Database ', ' Mobile ', 'Website '];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.categories = ['Agriculture And Veterinary ', 'Armed Force ', ' Art And Culture ', 'Automobile '];
        $scope.pricingmodels = [' CPC ', 'CPM ', 'CPV ', 'Fixed '];
        function knowMore(record){
            $location.url('/digitalKnowmore');
            recordDetails= record;
        }
        
        function getFilterLocation(arg){  
        	dataLength = 0;    
        	digitalList =[];      
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            }else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getDigital();       
        }

        function getFilterLang(arg){
        	dataLength = 0;
        	digitalList =[];
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            }else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            }
            getDigital();       
        }


		function getDigital(){			
			digitalControllerScope.loader = true;
			var parameters = {
				offset: dataLength	,
				sortBy: '_id',
				languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
        geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : []),
        languages:"",
        prisingmodel:"",
        medium:"",
        categories:""

			}
			digitalService.getDigitalDetail(parameters).then(function(data){				
				var results = JSON.parse(data);				
				if(results.response.success == 'true'){
            for(var i = 0; i < JSON.parse(results.response.results).length; i++){
                  digitalList.push(JSON.parse(results.response.results)[i]); 
                }
					dataLength += JSON.parse(results.response.results).length;
					$scope.count = results.response.count;
					$scope.records = digitalList;
          digitalControllerScope.count = dataLength;
					digitalControllerScope.loader =false;
				}	
			});
		}

		function dropboxitemselected(location){			
			dataLength = 0;
		}


		function addMore(){
            var count =  digitalControllerScope.regularOptions.length + 1;
            digitalControllerScope.regularOptions.push("mediaOption"+count);
        }
            
        digitalControllerScope.mediaOptions = ['costPerView','fixed','regularOptions'];
        digitalControllerScope.costPerView = ["mediaOption1"];
        digitalControllerScope.fixed = ["mediaOption1"];
        digitalControllerScope.regularOptions = ["mediaOption1"];
        function addNewDigital(){ 
            console.log("in  ", digitalControllerScope.options);               
            $location.url('/digitalNew');
        }

        var id= '';
        function addDigital(){                 
            digitalControllerScope.digital._id = id; 
            digitalControllerScope.digital.categoryId = [];
            console.log("chethannnn   ", digitalControllerScope.digital);
            digitalService.addDigital(digitalControllerScope.digital).then(function(data){                    
                var jsonFormatedData = JSON.parse(data);  
                console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$oid'];
                }
            },function(error){
                console.log("error",error)
            });  
        }
        $scope.currentTab = 'digitalGridView';








		// $scope.tabs = [{
  //           title: 'Grid View',
  //           url: 'digitalGridView',

  //       }, {
  //           title: 'List View',
  //           url: 'digitalListView'
  //       }];
  //       $scope.currentTab = 'digitalGridView';
  //       $scope.onClickTab = function (tab) {
  //           $scope.currentTab = tab.url;
  //           dataLength = 0 ;
  //           digitalList = []; 
  //           $scope.records = [];           
  //           getDigital();			          
  //       }        
  //       $scope.isActiveTab = function(tabUrl) {
  //           return tabUrl == $scope.currentTab;
  //       }

	}