angular
	.module('app')
	.controller('newspaperController', newspaperController);

	newspaperController.$inject = ['$scope', 'newspaperService', '$rootScope', '$location'];

	function newspaperController($scope, newspaperService, $rootScope, $location) {
		var newspaperControllerScope = this;
        newspaperControllerScope.loader = true;
        newspaperControllerScope.getNewspaper = getNewspaper;
        newspaperControllerScope.knowMore = knowMore;
        newspaperControllerScope.getFilterLocation = getFilterLocation;
        newspaperControllerScope.getFilterLang = getFilterLang;
        newspaperControllerScope.sortBy = sortBy;
        newspaperControllerScope.addNewNewspaper = addNewNewspaper;
        newspaperControllerScope.addNewspaper = addNewspaper;

        var newspaperList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = 'circulation';

        getNewspaper();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.categories = [' Retail ', 'Spiritual ', 'Sports ', 'Technology '];
        $scope.publications = [' Andhra Jyothi ', 'Andhra Prabha ', 'Asian Age ', ' Asomiya Pratidin '];
        function knowMore(record){
            $location.url('/newspaperKnowmore');
            recordDetails= record;
        }

        
         function sortBy(arg){  
            console.log("sort by", arg);
            dataLength = 0;  
            newspaperList = []; 
            if (arg == '') {
                sortBy = 'circulation';
            } else {
               sortBy = arg;
            }
            getNewspaper();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            newspaperList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getNewspaper();       
        }

        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];

        function getFilterLang(arg){
            dataLength = 0;  
            newspaperList = [];   
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getNewspaper();       
        }

        //get newspaper details
        function getNewspaper(){
            newspaperControllerScope.loader = true;
           var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                    geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : []),
                    categories: "",
                    publications: "",
                    targetGroups:"",
                    mediaOptions:"",
                    frequencies:""
            };

                newspaperService.getNewspaperDetail(filterPar).then(function(data){
                    console.log("data",data);
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    newspaperControllerScope.newspaperDetails = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                         newspaperList.push(JSON.parse(newspaperControllerScope.newspaperDetails)[i]); 
                        }    
                    $scope.count = jsonFormatedData.response.count;  
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    newspaperControllerScope.newspaperDetails = newspaperList;
                    newspaperControllerScope.count = dataLength;
                    newspaperControllerScope.loader = false;

                }

            },function(error){
                    console.log("error",error)
            })
        }
           $scope.currentTab = 'newsPaperGridView'; 


        // $scope.tabs = [{
        //         title: 'Grid View',
        //         url: 'newsPaperGridView'
        // }, {
        //         title: 'List View',
        //         url: 'newsPaperListView'
        // }];

        // $scope.currentTab = 'newsPaperGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     newspaperList = [];
        //     newspaperControllerScope.newspaperDetails = [];
        //     getNewspaper();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }



        newspaperControllerScope.mediaOptions = ['regularOptions', 'otherOptions' ];
        newspaperControllerScope.regularOptions = ["anyPage", "lastPage", "page3", "page1"];
        newspaperControllerScope.otherOptions = ["skyBus", "pointerAd", "jacketFrontInside", "jacketFrontPage"];

        

        console.log("out  ", newspaperControllerScope.mediaOptions);
        function addNewNewspaper(){ 
             console.log("in  ", newspaperControllerScope.options);               
            $location.url('/newspaperNew');
        }

        var id= '';
        function addNewspaper(){ 
            newspaperControllerScope.newspaper.categoryId = [];
            newspaperControllerScope.newspaper.geography = [];
            newspaperControllerScope.newspaper.urlSlug = '';
            newspaperControllerScope.newspaper.uniqueId = '';  
            newspaperControllerScope.newspaper._id = id; 
            console.log("chethannnn   ", newspaperControllerScope.newspaper);
            newspaperService.addNewspaper(newspaperControllerScope.newspaper).then(function(data){
                
                 var jsonFormatedData = JSON.parse(data);  
                 console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$oid'];
                }
            },function(error){
                console.log("error",error)
            });  
        }

		
	}

		

