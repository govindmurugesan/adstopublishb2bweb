angular
  .module('app')
  .controller('radioController', radioController);

  radioController.$inject = ['$scope', 'radioService', '$rootScope', '$location'];

  function radioController($scope, radioService, $rootScope, $location) {
    var radioControllerScope = this;
        radioControllerScope.loader=true;
        radioControllerScope.getRadio = getRadio;
        radioControllerScope.knowMore = knowMore;
        radioControllerScope.getFilterLocation = getFilterLocation;
        radioControllerScope.getFilterLang = getFilterLang;
        radioControllerScope.sortBy = sortBy;
       
        var radioList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = '';

        getRadio();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.stations = ['Aamar FM', 'Big FM', 'Club FM', 'Hit FM'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        radioControllerScope.mediaOptions = ['otherOptions','rjOptions','regularOptions'];
        radioControllerScope.otherOptions = ["timeCheck", "studioShift", "roadblock-NonPrimeTime", "roadblock-PrimeTime", "roadblock-AllDay", "sponsorshipTag", "contest", "halfPageHorizontal"];
        radioControllerScope.rjOptions = ["1"];
        radioControllerScope.regularOptions = ["primeTime", "nonPrimeTime", "mixedTime"];

            
        function knowMore(record){
            $location.url('/radioKnowmore');
            recordDetails = record;
        }
        
        function sortBy(arg){  
            dataLength = 0;  
            radioList = []; 
            if (arg == '') {
                sortBy = 'topserch';
            } else {
               sortBy = arg;
            }
            getRadio();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            radioList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getRadio();       
        }

        function getFilterLang(arg){
            dataLength = 0;  
            radioList = [];   
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getRadio();       
        }

        //get radio details
        function getRadio(){
         radioControllerScope.loader = true;
           var  filterPar = {
                    offset: dataLength,
                    sortBy: '',
                    languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                    geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : [])
            };

            radioService.getRadioDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    radioControllerScope.radioDetails = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                        radioList.push(JSON.parse(radioControllerScope.radioDetails)[i]); 
                        }
                     
                    $scope.count = jsonFormatedData.response.count;               
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    radioControllerScope.radioDetails = radioList;
                    radioControllerScope.count = dataLength;
                radioControllerScope.loader = false;

                }
            },function(error){
                    console.log("error",error)
            })
        }
         $scope.currentTab = 'radioGridView';
                

           
            








                // $scope.tabs = [{
                //         title: 'Grid View',
                //         url: 'radioGridView'
                // }, {
                //         title: 'List View',
                //         url: 'radioListView'
                // }];

                // $scope.currentTab = 'radioGridView';

                // $scope.onClickTab = function (tab) {
                //     $scope.currentTab = tab.url;
                //     dataLength = 0 ;
                //     radioList = [];
                //     radioControllerScope.radioDetails = [];
                //     getRadio();                     
                // }
                
                // $scope.isActiveTab = function(tabUrl) {
                //     return tabUrl == $scope.currentTab;
                // }
    
  }