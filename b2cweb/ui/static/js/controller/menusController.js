angular
	.module('app')
	.controller('menusController', menusController);

	menusController.$inject = ['$scope', '$localStorage', '$location', '$cookies','$rootScope'];

	function menusController($scope, $localStorage, $location,  $cookies,$rootScope) {
		var menusControllerScope = this;
		menusControllerScope.loginShow = true;		
		menusControllerScope.logout = logout;
		menusControllerScope.login = login;

		/*menusControllerScope.usrName = $localStorage.jwtToken.sub;	*/

		getCookiesInfo();
		function getCookiesInfo(){
			var cookiesDetail = $cookies.getObject('customerInfoCookies');
			if(cookiesDetail){
				menusControllerScope.userName = cookiesDetail.sub;
				$location.url('/radio');
			}else{
				$location.url('/');
			}
		}

		$rootScope.$on("CallParentMethod", function(){
          getCookiesInfo();
        });
	
		function logout(){
			/*$localStorage.jwtToken = '';
			$localStorage.token = '';
			menusControllerScope.usrName = '';*/
			$cookies.remove('customerInfoCookies');
			menusControllerScope.userName = ""
			$rootScope.orderDetails = [];
			/*$location.url('/topmenus');*/
			$location.url('/');
		}

		function login(){
			$scope.vm.divShow = "";
			$scope.vm.userLoginDetail = {};
			$scope.vm.userRegistrationDetail = {};
			$scope.vm.loginErrorDiv = false;
			$scope.vm.loginMessage = false;
			$('#loginDescp').modal('show');
			$scope.vm.divShow = "login";
			$scope.vm.useralreadyRegisteredMsg  = false;
		}
	}
