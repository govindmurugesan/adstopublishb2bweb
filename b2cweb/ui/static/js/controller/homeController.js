angular
	.module('app')
	.controller('homeController', homeController);
	
	homeController.$inject = ['$scope', 'homeService', '$localStorage', '$location', '$rootScope','$window','$cookies'];

	function homeController($scope, homeService, $localStorage, $location, $rootScope,$window,$cookies) {
		var homeControllerScope = this;
		
		/*homeControllerScope.login = login;
		homeControllerScope.register = register;
		homeControllerScope.forgetpass = forgetpass;
		homeControllerScope.close = close;
		homeControllerScope.newregister = newregister;
		homeControllerScope.signupUpdate = signupUpdate;*/

		
		$localStorage.jwtToken = [];
		

		$scope.divShow = "login";
		$scope.show = function(arg) {
			console.log("arg",arg)
			$scope.divShow = arg;
			console.log("$scope.divShow",$scope.divShow);
			homeControllerScope.userLoginDetail=  {};
			homeControllerScope.userRegistrationDetail = {};

			/*userLoginDetail.userName = "";
			userLoginDetail.userPassword = "";
			userRegistrationDetail.email = "";
			userRegistrationDetail.name = "";
			userRegistrationDetail.phone = "";
			userRegistrationDetail.password = "";
			userRegistrationDetail.confirmPassword = "";*/
			/*userRegistrationDetail.forgetpassword = "";*/
        }

		//close method
		function close(user){
			console.log("close method")
			$('#loginDescp').modal('hide');
			console.log("userLoginDetail",homeControllerScope.userLoginDetail);
			homeControllerScope.userLoginDetail =  [];
			homeControllerScope.userRegistrationDetail = [];
			console.log("userLoginDetail",homeControllerScope.userLoginDetail);
			/*$scope.divShow = "login";
			user.userName = "";
			user.userPassword = "";
			user.email = "";
			user.name = "";
			user.phone = "";
			user.password = "";
			user.confirmPassword = "";
			user.forgetpassword = "";*/

		}

		 function show (arg, user) {
			user.userName = "";
			user = [];
			
			$scope.divShow = arg;
			if(arg == 'signup'){
				console.log("sin")
				$('#loginDescp').modal('show');
				$('#signUpDescp').modal('show');
			}
			if(arg == 'login'){
				console.log("logn")
				$('#loginDescp').modal('show');
			}
			console.log("arg",$scope.divShow)
			console.log("loginuser",user);
			console.log("user.password",user.password);
			/*user.userName = "";
			user.userPassword = "";
			user.email = "";
			user.name = "";
			user.mobileNo = "";
			user.password = "";
			user.forgetpassword = "";*/
        }



		//login method
		function login(){
			console.log("login",homeControllerScope.userLoginDetail);
			console.log("g",homeControllerScope.userLoginDetail.username);
			console.log("vg",homeControllerScope.userLoginDetail.password);

			if((homeControllerScope.userLoginDetail.username != undefined) && (homeControllerScope.userLoginDetail.password != undefined)){
				$rootScope.spinner=true;
			homeService.login(homeControllerScope.userLoginDetail).then(
		        function(data) {
					var results = JSON.parse(data);
					console.log("results",results)
					var success = results.response.success;
						if(success == 'true'){

							var base64Url = $localStorage.token.split('.')[1];
							$rootScope.spinner=false;
							var base64 = base64Url.replace('-', '+').replace('_', '/');
							$localStorage.jwtToken = JSON.parse(atob(base64));
							/*$rootScope.usrName = $localStorage.jwtToken.sub*/
						
							var expireDate = new Date();
	  						expireDate.setDate(expireDate.getDate() + 7);
	  
							$cookies.putObject('customerInfoCookies', $localStorage.jwtToken, {'expires': expireDate});
							var userDetailsInCookies = $cookies.getObject('customerInfoCookies');
	           				$rootScope.$emit("CallParentMethod", {});
							$location.url('/newspaper');
						}else{
							/*user.loginFailed = true;*/
							$rootScope.spinner = false;
							$location.url('/error');
						}
						
		        },function(errResponse){

		             console.error('Error while fetching Users');
		        });
			
			}else{
				console.log("error in request")
			}
			

		}



		//register method
		function register(){			
				$rootScope.spinner = true;
				console.log("user",homeControllerScope.userRegistrationDetail);
			
			homeService.register(homeControllerScope.userRegistrationDetail).then(function(registerServiceResponsedata) {		 
			 	
			 	var results = JSON.parse(registerServiceResponsedata);	
			 	console.log("datadata",results);					
					 $localStorage.jwtToken="";
                       homeControllerScope.userRegistrationDetail = {};
					   user.confirmPassword = "";
                        var base64Url = $localStorage.token.split('.')[1];
                        var base64 = base64Url.replace('-', '+').replace('_', '/');
                        $localStorage.jwtToken = JSON.parse(atob(base64));
                        console.log($localStorage.jwtToken.sub);
                        $rootScope.usrName = $localStorage.jwtToken.sub
                        console.log($localStorage.jwtToken)
				if(results.response.success == 'true'){
				 	if(results.response.results == 'register'){
                        console.log("new registerd");
                         show('signup',user)
                         $rootScope.spinner = false;
                    }else{
                        $('#loginDescp').modal('hide');
                        console.log("true")
                        $localStorage.jwtToken="";
                        user.name="";
                        user.email="";
                        user.phone="";
                        user.password="";
                        user.confirmPassword="";
                    
                        var base64Url = $localStorage.token.split('.')[1];
                        
                        var base64 = base64Url.replace('-', '+').replace('_', '/');
                        $localStorage.jwtToken = JSON.parse(atob(base64));
                        console.log($localStorage.jwtToken.sub);
                        $rootScope.usrName = $localStorage.jwtToken.sub
                        console.log($localStorage.jwtToken)
                        $rootScope.usrId = $localStorage.jwtToken.jti;
                        $rootScope.spinner=false;
                        $location.url('/index');
                    }	
				}else{
					
					console.log("false");
			 		if(results.response.errors == 'already registerd'){
			 			console.log("registerd");
			 			show('login','');
			 			$rootScope.spinner = false;
			 		}
			 	}
				 
			 },
			 function(errResponse){
				 console.error('Error while fetching Users');
			 });
			
			}
		

		//forgot password method
		function forgetpass(user){
			$rootScope.spinner = true;
			console.log("My forgetpass Method called ...");
			homeService.forgetpass(user).then(function(data) {
	        	  	console.log("aaa", data);
	        	  	var results = JSON.parse(data);		
					var success = results.response.success;
					if(success == 'true'){
						
						//TODO modal should be closed here
						$location.url('/resetPassword');
					}else{
						// $rootScope.spinner = false;
						$location.url('/error');
					}
	          },
	          function(errResponse){
	              console.error('Error while fetching Users');
	          });
			$rootScope.spinner = false;
		}
          
          //signupUpdate method
		function signupUpdate(user){
			$rootScope.spinner = true;
			console.log("user", user);
			homeService.signupUpdate(user).then(function(data) {
	        	  	console.log("aaa", data);
	        	  	var results = JSON.parse(data);		
					var success = results.response.success;
					if(success == 'true'){
						//TODO modal should be closed here
						$location.url('/index');
					}else{
						// $rootScope.spinner = false;
						$location.url('/error');
					}
	          },
	          function(errResponse){
	              console.error('Error while fetching Users');
	          });
			$rootScope.spinner = false;
		}


		function  newregister() {
			console.log("srf");
		}
	}

// myApp.directive("compareTo", function() {
//    	return {
//        require: "ngModel",
//        link: function(scope, element, attrs, ctrl) {

//            ctrl.$validators.compareTo = function(val) {
//                return val == scope.$eval(attrs.compareTo);
//            };

//            scope.$watch(attrs.compareTo, function() {
//                ctrl.$validate();
//            });
//        }
//    };
// });


	