angular.module('app').controller('orderController', orderController)
    orderController.$inject = ['$scope', 'orderService', '$cookies']; 


        function orderController($scope, orderService, $cookies) {
        var orderControllerScope = this;
        orderControllerScope.loader = true;
        orderControllerScope.getMyOrder = getMyOrder;

        getMyOrder();
        function getMyOrder(){
         orderControllerScope.loader = true;
         var cookiesDetail = $cookies.getObject('customerInfoCookies');
          if(cookiesDetail){
             if(cookiesDetail.jti){
               var orderType = {
                 customerid: cookiesDetail.jti,
               }
               orderService.getProccedOrders(orderType).then(function(data){
                   var jsonFormatedData = JSON.parse(data);
                   console.log(jsonFormatedData)
                   if(jsonFormatedData.response.success == 'true'){
                     orderControllerScope.orderDetails =   JSON.parse(jsonFormatedData.response.results);
                     console.log(orderControllerScope.orderDetails);
                     orderControllerScope.loader = false;
                   }
               },function(error){
                   console.log("error",error);
                   orderControllerScope.loader = false;
               })
             }
           }
       }
        
}




		
