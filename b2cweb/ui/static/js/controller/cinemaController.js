angular
    .module('app')
    .controller('cinemaController', cinemaController);

    cinemaController.$inject = ['$scope', 'cinemaService', '$rootScope', '$location'];

    function cinemaController($scope, cinemaService, $rootScope, $location) {
        var cinemaControllerScope = this;
        cinemaControllerScope.loader = true;
        cinemaControllerScope.getCinemas = getCinemas;
        cinemaControllerScope.knowMore = knowMore;
        cinemaControllerScope.getFilterLocation = getFilterLocation;
        cinemaControllerScope.getFilterLang = getFilterLang;
        cinemaControllerScope.sortBy = sortBy;
              
        var cinemaList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = 'topsearch';

        getCinemas();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.cinemachains = ['A World ', ' Aarthi Grand ', 'Aasha ', 'Abirami '];
        $scope.mallnames = [' 3D Miniplex', ' A World', ' Aasha Cineplex', ' Abirami'];
        $scope.screentypes = ['Multiplex ', 'Single Screen '];

        function knowMore(record){
            $location.url('/cinemaKnowmore');
            recordDetails = record;
        }

        function sortBy(arg){  
            dataLength = 0;  
            cinemaList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getCinemas();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            cinemaList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getCinemas();       
        }

        function getFilterLang(arg){
            dataLength = 0;  
            cinemaList = [];   
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getCinemas();       
        }

        //get cinemas  details
        function getCinemas(){
             cinemaControllerScope.loader = true;
           var  filterPar = {
                offset: dataLength,
                sortBy: sortBy,
                languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : []),
                cinemaChain:"",
                mallName:"",
                screentype:"",
            };

            cinemaService.getCinemaDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    cinemaControllerScope.cinemaDetail = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                            cinemaList.push(JSON.parse(cinemaControllerScope.cinemaDetail)[i]); 
                        }
                    $scope.count = jsonFormatedData.response.count;
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    cinemaControllerScope.cinemaDetail = cinemaList;
                    cinemaControllerScope.count = dataLength;
                    cinemaControllerScope.loader = false;
                }

            },function(error){
                console.log("error",error)
            })
        }
        $scope.currentTab = 'cinemaGridView';


        // $scope.tabs = [{
        //         title: 'Grid View',
        //         url: 'cinemaGridView'
        // }, {
        //         title: 'List View',
        //         url: 'cinemaListView'
        // }];

        // $scope.currentTab = 'cinemaGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     cinemaList = [];
        //     cinemaControllerScope.cinemaDetail = [];
        //     getCinemas();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }
    
    }

        

