angular
    .module('app')
    .controller('outdoorController', outdoorController);

    outdoorController.$inject = ['$scope', 'outdoorService', '$rootScope', '$location'];

    function outdoorController($scope, outdoorService, $rootScope, $location) {
        var outdoorControllerScope = this;
        outdoorControllerScope.loader = true;
        outdoorControllerScope.getOutdoor = getOutdoor;
        outdoorControllerScope.knowMore = knowMore;
        outdoorControllerScope.getFilterLocation = getFilterLocation;
        outdoorControllerScope.getFilterLang = getFilterLang;
        outdoorControllerScope.sortBy = sortBy;
     
        //addnew
    
            $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
            $scope.mediatypes = ['Backlit Wall ', 'Bus Shelter ', 'Designer Wall ', ' Fuel Pump Hoarding '];
            $scope.sizes = ['Large ', 'Medium ', 'Small '];
            $scope.lits = ['BACK LIT ', ' FRONT LIT ', 'NON LIT '];


        var outdoorList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = 'topsearch';
        $scope.regularOptions = {
            mediaOptions:[
                {
                    area: "",
                    cardRate: "",
                    discountedRate: "",
                    imageUrl: "",
                    leadTime: "",
                    printingAndMountingRate: "",
                    ratePerSquareFeet: "",
                    vendorRate: ""
                }
            ]
        };

        getOutdoor();

            
        function knowMore(record){                
            $location.url('/outdoorKnowmore');
            recordDetails = record;
        }

        
        function sortBy(arg){  
            dataLength = 0;  
            outdoorList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getOutdoor();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            outdoorList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getOutdoor();       
        }

            function getFilterLang(arg){
                dataLength = 0;  
                outdoorList = [];   
                if ($scope.lang.indexOf(arg) === -1) {
                    $scope.lang.push(arg);
                } else {
                    $scope.lang.splice($scope.lang.indexOf(arg), 1);
                } 
                getOutdoor();       
            }

            //get Outdoor  details
            function getOutdoor(){
            outdoorControllerScope.loader = true;
               var  filterPar = {
                        offset: dataLength,
                        sortBy: sortBy,
                        languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                        geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : []),
                        mediaType:"",
                        size:""
                };

                outdoorService.getOutdoorDetail(filterPar).then(function(data){
                    var jsonFormatedData = JSON.parse(data);
                    if(jsonFormatedData.response.success == 'true'){
                        outdoorControllerScope.outdoorDetail = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                       outdoorList.push(JSON.parse(outdoorControllerScope.outdoorDetail)[i]); 
                         }
                        dataLength += JSON.parse(jsonFormatedData.response.results).length;
                        $scope.count = jsonFormatedData.response.count;
                        outdoorControllerScope.outdoorDetail = outdoorList;
                        outdoorControllerScope.count = dataLength;
                        outdoorControllerScope.loader = false;
                    }

                },function(error){
                        console.log("error",error)
                });
            }
             $scope.currentTab = 'outdoorGridView';

           


                // $scope.tabs = [{
                //         title: 'Grid View',
                //         url: 'outdoorGridView'
                // }, {
                //         title: 'List View',
                //         url: 'outdoorListView'
                // }];


               

                // $scope.currentTab = 'outdoorGridView';

                // $scope.onClickTab = function (tab) {
                //     $scope.currentTab = tab.url;
                //     dataLength = 0 ;
                //     outdoorList = [];
                //     outdoorControllerScope.outdoorDetail = [];
                //     getOutdoor();                     
                // }
                
                // $scope.isActiveTab = function(tabUrl) {
                //     return tabUrl == $scope.currentTab;
                // }


               

                

    }

        

