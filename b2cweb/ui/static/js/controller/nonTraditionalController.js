angular
    .module('app')
    .controller('nonTraditionalController', nonTraditionalController);

    nonTraditionalController.$inject = ['$scope', 'nonTraditionalService', '$rootScope', '$location'];

    function nonTraditionalController($scope, nonTraditionalService, $rootScope, $location) {
        var nonTraditionalControllerScope = this;
        nonTraditionalControllerScope.loader = true;
        nonTraditionalControllerScope.getNonTraditional = getNonTraditional;
        nonTraditionalControllerScope.knowMore = knowMore;
        nonTraditionalControllerScope.getFilterLocation = getFilterLocation;
        nonTraditionalControllerScope.getFilterLang = getFilterLang;
        nonTraditionalControllerScope.sortBy = sortBy;


        var nonTraditionalList = [];
        var dataLength = 0;
        $scope.place = [];
        $scope.lang = [];
        var sortBy = 'topsearch';

        getNonTraditional();

        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.categories = [' Food And Hospitality', 'Live Screens ', 'Others ', ' Print '];  
        nonTraditionalControllerScope.mediaOptions = ['regularOptions'];
        nonTraditionalControllerScope.regularOptions = ["mediaOption1"];
            
        function knowMore(record){
            $location.url('/nontraditionalKnowmore');
            recordDetails = record;
        }

            
        function sortBy(arg){  
            dataLength = 0;  
            nonTraditionalList = []; 
            if (arg == '') {
                sortBy = 'topsearch';
            } else {
               sortBy = arg;
            }
            getNonTraditional();  
        }

        function getFilterLocation(arg){  
            dataLength = 0;  
            nonTraditionalList = []; 
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getNonTraditional();       
        }

        function getFilterLang(arg){
            dataLength = 0;  
            nonTraditionalList = [];   
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getNonTraditional();       
        }

            //get nonTraditional  details
        function getNonTraditional(){
          nonTraditionalControllerScope.loader = true;
           var  filterPar = {
                    offset: dataLength,
                    sortBy: sortBy,
                    languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                    geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : [])
            };
            
            nonTraditionalService.getNonTraditionalDetail(filterPar).then(function(data){
                var jsonFormatedData = JSON.parse(data);
                if(jsonFormatedData.response.success == 'true'){
                    nonTraditionalControllerScope.nonTraditionalDetail = jsonFormatedData.response.results;
                    for(var i = 0; i < JSON.parse(jsonFormatedData.response.results).length; i++){
                     nonTraditionalList.push(JSON.parse(nonTraditionalControllerScope.nonTraditionalDetail)[i]); 
                        }
                    $scope.count = jsonFormatedData.response.count;
                    console.log($scope.count);
                    dataLength += JSON.parse(jsonFormatedData.response.results).length;
                    nonTraditionalControllerScope.nonTraditionalDetail = nonTraditionalList;
                    nonTraditionalControllerScope.count = dataLength;
                    nonTraditionalControllerScope.loader = false;
                }

            },function(error){
                    console.log("error",error)
            })
        }
        $scope.currentTab = 'nonTraditionalGridView';

        // $scope.tabs = [{
        //         title: 'Grid View',
        //         url: 'nonTraditionalGridView'
        // }, {
        //         title: 'List View',
        //         url: 'nonTraditionalListView'
        // }];

        // $scope.currentTab = 'nonTraditionalGridView';

        // $scope.onClickTab = function (tab) {
        //     $scope.currentTab = tab.url;
        //     dataLength = 0 ;
        //     nonTraditionalList = [];
        //     nonTraditionalControllerScope.nonTraditionalDetail = [];
        //     getNonTraditional();                     
        // }
        
        // $scope.isActiveTab = function(tabUrl) {
        //     return tabUrl == $scope.currentTab;
        // }
        
    }

        

