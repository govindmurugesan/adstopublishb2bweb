angular
.module('app')
.controller('myQuotesController', myQuotesController)
myQuotesController.$inject = ['$scope', '$rootScope', '$location','$cookies','orderService']; 

function myQuotesController($scope, $rootScope, $location,$cookies,orderService) {
    var myQuotesControllerScope = this;
    myQuotesControllerScope.loader = true;
    myQuotesControllerScope.result = '0';
    myQuotesControllerScope.getMyOrderQuotes = getMyOrderQuotes;
    myQuotesControllerScope.selectOption = selectOption;

    var selectedRecord = [];

    getOrderQuotes();
    myQuotesControllerScope.getMyOrderQuotes = getMyOrderQuotes;
    var dataLength = 0;
    
    getOrderQuotes();

    function getOrderQuotes(){
       myQuotesControllerScope.loader = true;
        var cookiesDetail = $cookies.getObject('customerInfoCookies');
        if(cookiesDetail){
            if(cookiesDetail.jti){
                var orderType = {
                    status: 'NW',
                    customerID: cookiesDetail.jti
                }
                orderService.getNewOrders(orderType).then(function(data){
                    var jsonFormatedData = JSON.parse(data);
                    if(jsonFormatedData.response.success == 'true'){
                        console.log("quotes list",JSON.parse(jsonFormatedData.response.results));
                        myQuotesControllerScope.quotesList =   JSON.parse(jsonFormatedData.response.results);
                        myQuotesControllerScope.loader = false;
                    }
                    myQuotesControllerScope.loader = false;
                },function(error){
                    console.log("error",error);
                })
            }
        }
    }

     function getMyOrderQuotes(orderid, $index){
            myQuotesControllerScope.getMyQuoteList = []
            var cookiesDetail = $cookies.getObject('customerInfoCookies');
            if (cookiesDetail) {
                if(cookiesDetail.jti){
                    var orderType = {
                       customerid: cookiesDetail.jti,
                       orderno: orderid
                    }
                    orderService.getMyOrderQuotes(orderType).then(function(data){
                        var jsonFormatedData = JSON.parse(data);
                        if(jsonFormatedData.response.success == 'true'){
                            myQuotesControllerScope.getMyQuoteList= JSON.parse(jsonFormatedData.response.results);
                        }
                    },
                    function(error){
                        console.log("error",error);
                    })
                }
            }
        }

         function selectOption(item, index){
            selectedRecord.push(item);
            selectedRecord.push(myQuotesControllerScope.getMyQuoteList[index]);
            $location.url('/bookingSummary');
            recordDetails = selectedRecord;
        }
}
 