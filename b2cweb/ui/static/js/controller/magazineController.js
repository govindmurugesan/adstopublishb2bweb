angular.module('app').controller('magazineController', magazineController);


	magazineController.$inject = ['$scope', 'magazineService', '$location', '$localStorage',  '$rootScope'];

	function magazineController($scope, magazineService, $location, $localStorage, $rootScope) {
		var magazineControllerScope = this;
    magazineControllerScope.loader = true;
		magazineControllerScope.getMagazine = getMagazine;
		magazineControllerScope.dropboxitemselected = dropboxitemselected;
		magazineControllerScope.knowMore = knowMore;
        magazineControllerScope.sortBy = sortBy;

		var dataLength = 0 ;		
		var magzinList =[];	
		$scope.place = [];
	    $scope.lang = [];
        var sortBy = 'circulation';	

        getMagazine();

         $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];
        $scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];
        $scope.categories = ['Agriculture And Veterinary ', 'Animation ', 'Armed Force ', ' Art And Culture '];
        $scope.targetgroups = ['Adult ', ' B2B ', ' B2C ', 'Female '];
        magazineControllerScope.mediaOptions = ['regularOptions'];
        magazineControllerScope.regularOptions = ["fullPage", "halfPage", "insideFrontCover", "insideBackCover", "backCover", "doubleSpread", "halfPageVertical", "halfPageHorizontal", "verticalStrip", "horizontalStrip", "frontGatefold", "centerDoubleSpread"];
       
        function knowMore(record){
            $location.url('/magazineKnowMore');
            recordDetails = record;
        }

        function sortBy(arg){  
            console.log("sort by", arg);
            dataLength = 0;  
            newspaperList = []; 
            if (arg == '') {
                sortBy = 'circulation';
            } else {
               sortBy = arg;
            }
            getMagazine();  
        }
        function getFilterLocation(arg){  
        	dataLength = 0;    
        	magzinList =[];      
            if ($scope.place.indexOf(arg) === -1) {
                $scope.place.push(arg);
            } else {
                $scope.place.splice($scope.place.indexOf(arg), 1);
            }
            getMagazine();       
        }

        function getFilterLang(arg){
        	dataLength = 0;
        	magzinList =[];
            if ($scope.lang.indexOf(arg) === -1) {
                $scope.lang.push(arg);
            } else {
                $scope.lang.splice($scope.lang.indexOf(arg), 1);
            } 
            getMagazine();       
        }


		function getMagazine(){
			magazineControllerScope.loader = true;
			var parameters = {
				offset: dataLength	,
				languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
                geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : [])
			}

			magazineService.getMagazine(parameters).then(function(data){				
				var results = JSON.parse(data);
				if(results.response.success == 'true'){
          for(var i = 0; i < JSON.parse(results.response.results).length; i++){
              magzinList.push(JSON.parse(results.response.results)[i]);
            }
					$scope.count = results.response.count;  
					dataLength += JSON.parse(results.response.results).length;
					$scope.records = magzinList;
          magazineControllerScope.count = dataLength;
					 magazineControllerScope.loader = false;
				}					

			});
		}


		function dropboxitemselected(location){			
			dataLength = 0;
		}
           $scope.currentTab = 'radioGridView';
        
		// $scope.tabs = [{
  //           title: 'Grid View',
  //           url: 'radioGridView',

  //       }, {
  //           title: 'List View',
  //           url: 'radioListView'
  //       }];
        
  //       $scope.currentTab = 'radioGridView';
  //       $scope.onClickTab = function (tab) {
  //           $scope.currentTab = tab.url;
  //           dataLength = 0 ;
  //           magzinList = []; 
  //           $scope.records = [];
  //           console.log(magzinList + '   =   ' +$scope.records);
  //           getMagazine();			          
  //       }        
  //       $scope.isActiveTab = function(tabUrl) {
  //           return tabUrl == $scope.currentTab;
  //       }

	}