angular.module('app').controller('airlineAndAirportController', airlineAndAirportController);

	airlineAndAirportController.$inject = ['$scope', 'airlineAndAirportService', '$location', '$localStorage',  '$rootScope'];

	function airlineAndAirportController($scope, airlineAndAirportService, $location, $localStorage, $rootScope) {
		var airlineAndAirportControllerScope = this;
		airlineAndAirportControllerScope.loader = true;
		airlineAndAirportControllerScope.getAirport = getAirport;
		airlineAndAirportControllerScope.dropboxitemselected = dropboxitemselected;
		airlineAndAirportControllerScope.knowMore = knowMore;
		airlineAndAirportControllerScope.addAirport = addAirport;
		airlineAndAirportControllerScope.getFilterLocation = getFilterLocation;
		airlineAndAirportControllerScope.getFilterLang = getFilterLang;
		airlineAndAirportControllerScope.sortBy = sortBy;
		airlineAndAirportControllerScope.addNewAirline = addNewAirline;
		airlineAndAirportControllerScope.addAirline = addAirline;
		airlineAndAirportControllerScope.addMore = addMore;
		
        
		var dataLength = 0 ;
		var airlineAirports = [];
		$scope.records = [];
		$scope.place = [];
		$scope.lang = [];
		var sortBy = 'topserch';

		function sortBy(arg){  
            console.log("sort by", arg);
            dataLength = 0;  
            airlineAirports = []; 
            if (arg == '') {
                sortBy = 'topserch';
            } else {
               sortBy = arg;
            }
            getAirport();  
        }

		function getFilterLocation(arg){	
		    airlineAirports = [];		
		    dataLength = 0 ;
			if ($scope.place.indexOf(arg) === -1) {
	            $scope.place.push(arg);
	        } else {
	            $scope.place.splice($scope.place.indexOf(arg), 1);
	        }
	        console.log($scope.place)
				
			getAirport();		
		}

		$scope.languages = ['Kannada', 'English', 'Hindi', 'Marathi'];

		function getFilterLang(arg){

			airlineAirports = [];
			dataLength = 0 ;
			if ($scope.lang.indexOf(arg) === -1) {
	            $scope.lang.push(arg);
	        } else {
	            $scope.lang.splice($scope.lang.indexOf(arg), 1);
	        }
	        console.log($scope.lang)
				
			getAirport();		
		}

		
		function knowMore(record){			
			$location.url('/airlineAirportKnowmore');
			recordDetails = record;
		}

        getAirport();
		function getAirport(){
			airlineAndAirportControllerScope.loader = true;
			console.log('sortby  ',sortBy);
			var parameters = {
				offset: dataLength	,
				sortBy: sortBy,
				languages: ($scope.lang.length > 0 && $scope.lang.length != undefined ? $scope.lang : []),
				geographies: ($scope.place.length > 0 && $scope.place.length != undefined ? $scope.place : [])
			}
		
			airlineAndAirportService.getAirport(parameters).then(function(data){				
				var results = JSON.parse(data);
				console.log(results);
				console.log("results", results.response.success);
				if(results.response.success == 'true'){
					for(var i = 0; i < JSON.parse(results.response.results).length; i++){
                            airlineAirports.push(JSON.parse(results.response.results)[i]); 
                        }
					
					dataLength += JSON.parse(results.response.results).length;
					$scope.count = results.response.count;
					console.log($scope.count);
					airlineAndAirportControllerScope.airlineAirportsDetails = airlineAirports;
					airlineAndAirportControllerScope.count = dataLength;
					airlineAndAirportControllerScope.loader = false;
				}
				
			

			});
		}

		function addAirport(airportDetails){
			console.log("Airport start");
			$rootScope.spinner = true;
			airlineAndAirportService.addAirport().then(function(data){				
				var results = JSON.parse(data);
				console.log("results", results.response.success);
				if(results.response.success == 'true'){
					$scope.records = results.response.results;	
				}
				$rootScope.spinner=false;
			});
		}

		function UpdateAirport(airportDetails){
			console.log("Airport start");
			$rootScope.spinner = true;
			airlineAndAirportService.updateAirport().then(function(data){				
				var results = JSON.parse(data);
				console.log("results", results.response.success);
				if(results.response.success == 'true'){
					$scope.records = results.response.results;	
				}
				$rootScope.spinner=false;
			});
		}
		function dropboxitemselected(location){			
			dataLength = 0;
		}

		function addMore(optionType){
			var count = 1 + airlineAndAirportControllerScope[optionType].length;
			airlineAndAirportControllerScope[optionType].push('mediaOption'+count);
		}

		airlineAndAirportControllerScope.mediaOptions = ['digitalOptions','printOptions','aircraftOptions','popularOptions'];
        airlineAndAirportControllerScope.digitalOptions = ['mediaOption1'];
        airlineAndAirportControllerScope.printOptions = ['mediaOption1'];
        airlineAndAirportControllerScope.aircraftOptions = ['mediaOption1'];
        airlineAndAirportControllerScope.popularOptions = ['mediaOption1'];
       	function addNewAirline(){
			$location.url('/airlineAndAirportNew');
		}

		var id= '';
        function addAirline(){
        	airlineAndAirportControllerScope.airline.categoryId = [];
            airlineAndAirportControllerScope.airline.geography = [];  
            airlineAndAirportControllerScope.airline.keywords = [];           
            airlineAndAirportControllerScope.airline.uniqueId = '';  
            airlineAndAirportControllerScope.airline.thumbnail = ''; 
            airlineAndAirportControllerScope.airline.views = '1'; 
            airlineAndAirportControllerScope.airline._id = id; 
            console.log("chethannnn   ", airlineAndAirportControllerScope.airline);
            airlineAndAirportService.addAirline(airlineAndAirportControllerScope.airline).then(function(data){                
                var jsonFormatedData = JSON.parse(data);  
                console.log('jsonFormatedData ',jsonFormatedData);                      
                if(jsonFormatedData.response.success == 'true'){                           
                   var respDate = JSON.parse(jsonFormatedData.response.results)[0];
                   console.log('respDats', respDate);
                   id = respDate._id['$oid'];
                }
            },function(error){
                console.log("error",error)
            });  
        }

		$scope.categories = [' Airline ', 'Airport ', 'Airport Lounge ', 'Inflight Magazine'];
        $scope.locations = ['Bangalore', 'Chennai', 'Hyderabad', 'Chandigarh'];

		$scope.currentTab = 'radioGridView';
		
		// $scope.tabs = [{
  //           title: 'Grid View',
  //           url: 'radioGridView',

  //       }, {
  //           title: 'List View',
  //           url: 'radioListView'
  //       }];
  //       $scope.currentTab = 'radioGridView';
  //       $scope.onClickTab = function (tab) {
  //           $scope.currentTab = tab.url;
  //           dataLength = 0 ;
  //           getAirport();
		// 	$scope.records = [];           
  //       }        
  //       $scope.isActiveTab = function(tabUrl) {

  //           return tabUrl == $scope.currentTab;
  //       }

	}