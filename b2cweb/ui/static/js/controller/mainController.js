angular
	.module('app')
	.controller('mainController', mainController);

	mainController.$inject = ['$scope', 'constantsService', '$rootScope', 'homeService', '$localStorage', '$cookies', '$location', 'orderService'];

	function mainController($scope, constantsService,  $rootScope, homeService, $localStorage,  $cookies, $location, orderService) {
		var vm = this;
		vm.staticSource = constantsService.staticSource;
		vm.show = show;
		vm.login = login;
		vm.placeOrder = placeOrder;
		vm.register = register;
		/*vm.forgetpass = forgetpass;*/
		vm.close = close;
		vm.loginMessage = false;
		vm.useralreadyRegisteredMsg  = false;
		vm.today = today;
		vm.clear = clear;
		vm.open1 = open1;
		vm.divShow = "login";
		vm.loginErrorDiv = false;
		vm.userRegistrationDetail = {}
		

		//close method
		function close(){
			vm.userLoginDetail =  {};
			vm.userRegistrationDetail = {};
			vm.divShow = "";
			$('#loginDescp').modal('hide');
			$('.modal-backdrop').remove();

		}

		 function show (arg) {
		 	
			/*vm.divShow = "";*/
			vm.divShow = arg;
			vm.userLoginDetail =  {};
			vm.userRegistrationDetail = {};
			/*if(arg == 'signup'){
				console.log("sin")
				$('#loginDescp').modal('show');
				$('#signUpDescp').modal('show');
			}*/
			if(arg == 'login'){
				console.log("logn")
				$('#loginDescp').modal('show');
			}
        }

        //login method
		function login(){
			vm.loginErrorDiv = false;
			vm.loginMessage = false;
			if(vm.userLoginDetail){
				if((vm.userLoginDetail.username != undefined) && (vm.userLoginDetail.password != undefined)){
					$rootScope.spinner=true;
					homeService.login(vm.userLoginDetail).then(
			        function(data) {
						var results = JSON.parse(data);
						var success = results.response.success;
							if(success == 'true'){
								var base64Url = $localStorage.token.split('.')[1];
								$rootScope.spinner=false;
								var base64 = base64Url.replace('-', '+').replace('_', '/');
								$localStorage.jwtToken = JSON.parse(atob(base64));							
								var expireDate = new Date();
		  						expireDate.setDate(expireDate.getDate() + 7);		  
								$cookies.putObject('customerInfoCookies', $localStorage.jwtToken, {'expires': expireDate});
								var userDetailsInCookies = $cookies.getObject('customerInfoCookies');
		           				$rootScope.$emit("CallParentMethod", {});
		           				if($rootScope.orderDetails){
		           					if($rootScope.orderDetails.length>0){
			           					if(userDetailsInCookies.jti){
			           						 $scope.vm.loginMessage = false
			           						$rootScope.orderDetails[0].customerID = userDetailsInCookies.jti;
			           						vm.orderComments = "";
			           						vm.divShow = 'orderCommentsSection';
			           					}
			           				}else{
			           					$('#loginDescp').modal('hide');
			           					$('.modal-backdrop').remove();
		           						$location.url('/newspaper');
			           				}
		           				}else{
		           					$('#loginDescp').modal('hide');
			           					$('.modal-backdrop').remove();
		           						$location.url('/newspaper');
	       						}
								
							}else{
								vm.userLoginDetail = {};
								$rootScope.spinner = false;
								vm.loginErrorDiv = true;
							}
							
			        },function(errResponse){
			        	vm.loginErrorDiv = true;
			             console.error('Error while fetching Users',errResponse);
			        });
			
			}else{
				vm.loginErrorDiv = true;
				console.log("error in request")
			}
				}else{
					vm.loginErrorDiv = true;
					console.log("error in request")
				}
		}

		function placeOrder(orderComment){
			console.log("sdcs");
			vm.divShow = "";
			vm.loader= true;
			$rootScope.orderDetails[0].comments = orderComment;
			orderService.addOrder($rootScope.orderDetails[0]).then(function(data){
				vm.userLoginDetail =  {};
				vm.divShow = 'orderSuccess';
				$rootScope.orderDetails = [];
				vm.loader = false;
            },function(error){
                    console.log("error",error);
                    vm.divShow = 'orderfail';
                   
            })
		}



		function register(){			
					console.log("user", vm.userRegistrationDetail);
					if(vm.userRegistrationDetail != undefined){
						vm.userRegistrationDetail.name = vm.userRegistrationDetail.username;
						console.log("vm.passwordConfirmation.confirmPassword", vm.passwordConfirmation.confirmPassword);
					 if(vm.userRegistrationDetail.password == vm.passwordConfirmation.confirmPassword){
						homeService.register(vm.userRegistrationDetail).then(function(registerServiceResponsedata) {		 
						 	
						 	var results = JSON.parse(registerServiceResponsedata);	
						 	console.log("datadata",results.response.errors);					
							 	
							if(results.response.success == 'true'){
							 	if(results.response.results == 'register'){
			                        console.log("new registerd");

			                        vm.passwordConfirmation.confirmPassword = "";
			                        var base64Url = $localStorage.token.split('.')[1];
								$rootScope.spinner=false;
								var base64 = base64Url.replace('-', '+').replace('_', '/');
								$localStorage.jwtToken = JSON.parse(atob(base64));							
								var expireDate = new Date();
		  						expireDate.setDate(expireDate.getDate() + 7);		  
								$cookies.putObject('customerInfoCookies', $localStorage.jwtToken, {'expires': expireDate});
								var userDetailsInCookies = $cookies.getObject('customerInfoCookies');
		           				$rootScope.$emit("CallParentMethod", {});
			                         // store cookies detail
			                        $('#loginDescp').modal('hide');
									$('.modal-backdrop').remove();
									  $location.url('/newspaper');
			                         $rootScope.spinner = false;
			                    }else{
			                        $('#loginDescp').modal('hide');
			                        console.log("true")
			                        $localStorage.jwtToken="";
			                        user.name="";
			                        user.email="";
			                        user.phone="";
			                        user.password="";
			                        user.confirmPassword="";
			                    
			                        var base64Url = $localStorage.token.split('.')[1];
			                        
			                        var base64 = base64Url.replace('-', '+').replace('_', '/');
			                        $localStorage.jwtToken = JSON.parse(atob(base64));
			                        console.log($localStorage.jwtToken.sub);
			                        $rootScope.usrName = $localStorage.jwtToken.sub
			                        console.log($localStorage.jwtToken)
			                        $rootScope.usrId = $localStorage.jwtToken.jti;
			                        $rootScope.spinner=false;
			                        $location.url('/index');
			                    }	
							}else{
								console.log("false");
								console.log("iff",results.response.errors == 'Plz Login..already registerd');
						 		if(results.response.errors == 'Plz Login..already registerd'){
						 			console.log("registerd");
						 			show('login');
						 			vm.useralreadyRegisteredMsg  = true;
						 			$rootScope.spinner = false;
						 		}
						 	}
							 
						 },
						 function(errResponse){
							 console.error('Error while fetching Users');
						 });	
					}else{
						vm.errorinconfirmpassword = true;
							console.log("error in confirm pass")
					}
					}else{
							console.log("error regist detail")
					}
				}

		//register method
		// function register(user){			
		// 		$rootScope.spinner = true;
		// 		console.log("vm.userRegistrationDetail",vm.userRegistrationDetail)
		// 		console.log("user",vm.userRegistrationDetail);
			
		// 	homeService.register(vm.userRegistrationDetail).then(function(registerServiceResponsedata) {		 
			 	
		// 	 	var results = JSON.parse(registerServiceResponsedata);	
		// 	 	console.log("datadata",results);					
		// 			 $localStorage.jwtToken="";
  //                      vm.userRegistrationDetail = {};
		// 			   user.confirmPassword = "";
  //                       var base64Url = $localStorage.token.split('.')[1];
  //                       var base64 = base64Url.replace('-', '+').replace('_', '/');
  //                       $localStorage.jwtToken = JSON.parse(atob(base64));
  //                       console.log($localStorage.jwtToken.sub);
  //                       $rootScope.usrName = $localStorage.jwtToken.sub
  //                       console.log($localStorage.jwtToken)
		// 		if(results.response.success == 'true'){
		// 		 	if(results.response.results == 'register'){
  //                       console.log("new registerd");
  //                        show('signup',user)
  //                        $rootScope.spinner = false;
  //                   }else{
  //                       $('#loginDescp').modal('hide');
  //                       console.log("true")
  //                       $localStorage.jwtToken="";
  //                       user.name="";
  //                       user.email="";
  //                       user.phone="";
  //                       user.password="";
  //                       user.confirmPassword="";
                    
  //                       var base64Url = $localStorage.token.split('.')[1];
                        
  //                       var base64 = base64Url.replace('-', '+').replace('_', '/');
  //                       $localStorage.jwtToken = JSON.parse(atob(base64));
  //                       console.log($localStorage.jwtToken.sub);
  //                       $rootScope.usrName = $localStorage.jwtToken.sub
  //                       console.log($localStorage.jwtToken)
  //                       $rootScope.usrId = $localStorage.jwtToken.jti;
  //                       $rootScope.spinner=false;
  //                       $location.url('/index');
  //                   }	
		// 		}else{
					
		// 			console.log("false");
		// 	 		if(results.response.errors == 'already registerd'){
		// 	 			console.log("registerd");
		// 	 			show('login','');
		// 	 			$rootScope.spinner = false;
		// 	 		}
		// 	 	}
				 
		// 	 },
		// 	 function(errResponse){
		// 		 console.error('Error while fetching Users');
		// 	 });
			
		// 	}
		
		function today() {
            $scope.dt = new Date();
          };
        today();

         function clear() {
            $scope.dt = null;
          };

          $scope.dateOptions = {

            formatYear: 'yy',
            maxDate: new Date(3030, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };

          function open1() {
            $scope.popup1.opened = true;
         
          };
          $scope.popup1 = {
            opened: false
          };
           $scope.selectedDates = [new Date().setHours(0, 0, 0, 0), new Date(2015, 2, 20).setHours(0, 0, 0, 0), new Date(2015, 2, 10).setHours(0, 0, 0, 0), new Date(2015, 2, 15).setHours(0, 0, 0, 0)];

    

	}

